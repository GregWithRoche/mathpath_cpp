#include "CellSystemEvolver.h"

#include <cstdio>
#include <iostream>
#include <vector>
#include <vector>
#include <assert.h>
#include <random>
#include <memory>
#include <fstream>
#include <iterator>
#include <numeric>
#include <functional>

#include <glm/glm.hpp>

#include <nlohmann/json.hpp>

#include <vil_image_view.h>
#include <vil_distance_transform.h>
#include <vil_structuring_element.h>
#include <vil_binary_dilate.h>
#include <vil_binary_closing.h>

#include "Cell2d.h"
#include "RandomPermutation.h"
#include "Chronograph.h"

#define SUSPICIOUS_NEW_CELL_PROLIFERATION_CAPACITY
//#define DEBUG_DUMP_HYPOXIA_GRID
//#define DEBUG_DUMP_STRUCTURING_ELEMENT

using namespace glm;
using namespace nlohmann;

//#define NDEBUG true

// System state numbers.
// TODO: IMPORTANT encapsulate these if running many threads.
static unsigned tumorProliferatingNumber = 0u;
static unsigned tumorMovingNumber = 0u;
static unsigned tumorKilledNumber = 0u;
static unsigned tumorProliferatingExhausted = 0u;
static unsigned tumorDiedSpontaneouslyNumber = 0u;
static unsigned tumorDiedTurnedToNecrosis = 0u;

static unsigned immuneNewNumber = 0u;
static unsigned immuneProliferatingNumber = 0u;
static unsigned immuneAttackingNumber = 0u;
static unsigned immuneEngagedNumber = 0u;
static unsigned immuneProliferatingExhausted = 0u;
static unsigned immuneDiedSpontaneouslyNumber = 0u;
static float    immuneAveageMovingSpeed = 0u;
static unsigned immuneTurnedToFibrosis = 0u;

// Necrosis and fibrosis default seed fraction.
static const float seedFrac = 0.3;

static const vector<Index2D> surroundingHelper = {{-1, 1}, {0, 1}, { 1, 1},
                                                  {-1, 0},         { 1, 0},
                                                  {-1,-1}, {0,-1}, { 1,-1}};

static vil_structuring_element necrosisSeedDisk;
static vil_structuring_element necrosisGrowthDisk;
static vil_structuring_element fibrosisGrowthDisk;
static bool isVilStructuringElementInitialized = false;

// Initialize static Random class members.
std::random_device Random::r = std::random_device();
std::default_random_engine Random::generator(r());

static void debugSuspiciousProliferationCapacity(const CellPointers& cells, const SharedCell& newCell, const CellOccupationGrid& occupationGrid)
{
#ifdef SUSPICIOUS_NEW_CELL_PROLIFERATION_CAPACITY
    assert(newCell);
    assert(newCell->isTumor() || newCell->isImmune());

    const float highProliferationCapacity = 1.0 - (1.0/(float)Cell2D::getTumorProliferationMax());
    const int newCellProliferationCapacity = newCell->getProliferationCapacity();
    if ((newCell->getColor().r > highProliferationCapacity) || (newCellProliferationCapacity < 0))
    {
        // Create surroundingIndexes if within the grid and check for free spots
        CellPointers neighbors;
        bool isNeighborWithIdenticalProliferationCapacity = false;
        for (const auto& relative : surroundingHelper)
        {
            // TODO: reuse getCellSurrounding
            Index2D index = Index2D((int)round(newCell->getPosition().x), (int)round(newCell->getPosition().y)) + relative;
            if (index.x >=0 && (unsigned)index.x < occupationGrid[0].size() &&
                index.y >=0 && (unsigned)index.y < occupationGrid.size())
            {
                SharedCell neighborCell = occupationGrid[index.y][index.x];
                if (!neighborCell)
                    continue;

                neighbors.push_back(neighborCell);
                isNeighborWithIdenticalProliferationCapacity |= (neighborCell->getProliferationCapacity() == newCellProliferationCapacity);
            } 
        }

        if (isNeighborWithIdenticalProliferationCapacity && (newCellProliferationCapacity >= 0))
            return;

        // If no neighbor with the same proliferation capacity found, log this situation.
        std::cout << " #######################################################" << std::endl;
        std::cout << " ### NEW CELL SUSPICIOUS PROLIFERATION CAPACITY: " << newCellProliferationCapacity << " ###" << std::endl;
        newCell->dump();
        std::cout << " ### Its neighbors are:                              ###" << std::endl;
        for (const SharedCell& neighbor: neighbors)
        {
            neighbor->dump();
        }
        std::cout << " #######################################################" << std::endl;
        CellSystemEvolver::dumpSystem(cells);
        CellSystemEvolver::calculateDumpGridOccupation({&cells}, {occupationGrid.size(), occupationGrid[0].size()});
        std::cout << " #######################################################" << std::endl;
        assert(false);
    }
#endif
}

template<class T>
static auto setVilGridValue = [](vil_image_view<T>& grid, unsigned i, unsigned j, float value) {
    *(grid.begin() + i*grid.istep() + j*grid.jstep()) = value;
};

template<class T>
static auto getVilGridValue = [](const vil_image_view<T>& grid, unsigned i, unsigned j) {
    return *(grid.begin() + i*grid.istep() + j*grid.jstep());
};

CellSystemEvolver::CellSystemEvolver() :
    _isInitialized(false),
    _steps(0),

    _inputTumorCells{},
    _inputImmuneCells{},
    _inputNecrosisCells{},
    _inputFibrosisCells{},

    // Tumor parameters.
    //    tumorSymmetricDivisionProbability(0.76),
    _tumorSymmetricDivisionProbability(0.70),
    _tumorProliferationProbability(0.5055),
    _tumorProliferationMax(10),
    _tumorMigrationProbability(0.35),
    _tumorDeathProbability(1.0 - pow(1.0-0.0319, 4)),

    // Immune cells' parameters.
    _immuneKillMax(5),
    _immuneProliferationMax(10),
    _immuneProliferationProbability(0.0449),
    _immuneMigrationProbability(0.8),
    _immuneKillingProbability(0.5),
    _immuneDeathProbability(1.0 - pow(1.0-0.0037, 4)),
    _immuneRandomWalkFactor(0.8),
    _immuneSpeedMax(97),
    _immuneFlowProb(0.6),
    _immuneFlowRate(1),
    _immuneKillingEngagementDuration(48),

    // Necrosis parameters.
    _necrosisDistMax(134),
    _necrosisProbSeed(0.0),
//    _necrosisProbSeed(0.00004),
    _necrosisFraction(seedFrac),

    // Fibrosis parameters.
    _fibrosisProbSeed(0.00),
//    _fibrosisProbSeed(0.00025),
    _fibrosisFraction(seedFrac),
    _fibrosisStromaPermeability(0.0025)
{
    initializeVilStructuringElements();

    // TODO: We need to set it upfront, but this necessity doesn't seem to fit here.
    Cell2D::setImmuneKillingEngagementDuration(_immuneKillingEngagementDuration);
    Cell2D::setImmuneProliferationMax(_immuneProliferationMax);
    Cell2D::setImmuneKillingCapacityMax(_immuneKillMax);
}

CellSystemEvolver::CellSystemEvolver(const std::vector<Cell2D>& systemStateIn, const Index2D& gridSize) : CellSystemEvolver()
{
    initializeInPlace(systemStateIn, gridSize);
}

CellSystemEvolver::CellSystemEvolver(const string& systemJsonFilePath) : CellSystemEvolver()
{
    initializeFromFile(systemJsonFilePath);
}

void CellSystemEvolver::initializeVilStructuringElements()
{
    if (isVilStructuringElementInitialized)
        return;

    constexpr unsigned necrosisDilationDiskRadius = 4u;
    constexpr unsigned fibrosisDilationDiskRadius = 4u;
    necrosisSeedDisk.set_to_disk(necrosisDilationDiskRadius);
    necrosisGrowthDisk.set_to_disk(necrosisDilationDiskRadius);
    fibrosisGrowthDisk.set_to_disk(fibrosisDilationDiskRadius);
    isVilStructuringElementInitialized = true;
}

void CellSystemEvolver::initializeInPlace(const std::vector<Cell2D>& systemStateIn, const Index2D& gridSize)
{
    //TODO: Allow for dynamic resizing.
    assert(gridSize.x > 0);
    assert(gridSize.y > 0);

    _gridSize = gridSize;

    const unsigned systemSize = systemStateIn.size();
    for (unsigned int i = 0; i < systemSize; ++i)
    {
        SharedCell cell = std::make_shared<Cell2D>();
        *cell = systemStateIn[i];

        if (cell->isTumor())
        {
            _inputTumorCells.push_back(cell);
        }
        else if (cell->isImmune())
        {
            _inputImmuneCells.push_back(cell);
        }
        else if (cell->isFibrosis())
        {
            _inputFibrosisCells.push_back(cell);
        }
        else if (cell->isNecrosis())
        {
            _inputNecrosisCells.push_back(cell);
        }
    }

    if (systemSize == 1)
    {// For one initial cell, remember the maximum value of proliferation capacity.
        assert(_inputTumorCells.back()->isTumor());
        Cell2D::setTumorProliferationMax(_inputTumorCells.back()->getProliferationCapacity());
    }
    else
    {
        Cell2D::setTumorProliferationMax(_tumorProliferationMax);
    }
    _isInitialized = true;
}

void CellSystemEvolver::initializeFromFile(const string& systemJsonFilePath)
{
    Chronograph chrono(__FUNCTION__, true);

    std::ifstream fileStream;
    fileStream.open(systemJsonFilePath);
    if (!fileStream) {
        std::cerr << "Unable to open file: " << systemJsonFilePath << std::endl;
        exit(1);   // call system to stop
    }

    const std::string jsonSerializedTextString(std::istreambuf_iterator<char>{fileStream}, {});
    const json MySystem = json::parse(jsonSerializedTextString);

    std::cout << " ### The cell system desribed by file: \'" << systemJsonFilePath << "\' consists of:" << std::endl;
    auto range = [](const unsigned N) {
        std::vector<unsigned> out(N);
        std::iota(out.begin(), out.end(), 0);
        return out;
    };
    auto printJsonKeys = [&range](const json& j, const auto& lambda, const unsigned nesting = 0u) -> void {
        for (auto& [key, value] : j.items())
        {
          for (unsigned i : range(nesting))
          {
              std::cout << "  ";
          }

          json jin = j[key];
          std::cout << key;
          if (j[key].is_array())
          {
              std::cout << " array of size: " << value.size();
              if (j[key][0].is_array())
              {
                  // TODO: This is not universal, only works for 2-dim arrays.
                  std::cout << "x" << j[key][0].size();
              }
              std::cout << std::endl;
          }
          else if (j[key].is_number() || j[key].is_string())
          {
              std::cout << " " << value << std::endl;
          }
          else
          {
              std::cout << std::endl;
              lambda(jin, lambda, nesting +1);
          }
        }
    };
    printJsonKeys(MySystem, printJsonKeys);

    const json grid = MySystem["grid"];
    assert(grid["StepsDone"] >= 0);
    _steps = grid["StepsDone"];
    _gridSize = {grid["M"], grid["N"]};
    assert(_gridSize.x > 0);
    assert(_gridSize.y > 0);

    auto initializeCells = [&range](const json& grid, const string cellsId, CellPointers& newCells, std::function<Cell2D(Index2D&&)> initializer) {
        newCells.clear();
        json Ln = grid[cellsId];
        for (const unsigned row : range(Ln.size()))
        {
            for (const unsigned col : range(Ln[row].size()))
            {
                if (!Ln[row][col])
                    continue;
                newCells.push_back(make_shared<Cell2D>(initializer({col, row})));
            }
        };
        std::cout << " ### Initialized \'"<< cellsId << "\' with " << newCells.size() << " cells." << std::endl;
    };
    std::cout << std::endl;
    initializeCells(grid, "Ln", _inputNecrosisCells, &Cell2D::createNecrosisCell);
    initializeCells(grid, "Lf", _inputFibrosisCells, &Cell2D::createFibrosisCell);

    std::cout << std::endl;
    const json TU = MySystem["TU"];
    const json TUcells = TU["TUcells"];
    const json TUprop = TU["TUprop"];
    const json tumorPcap = TUprop["Pcap"];
    const json isStem = TUprop["isStem"];
    // TODO: This is causing problems again, as needs to be remembered to be called.
    //       And is called the same, should be in one place.
    _tumorProliferationMax = *std::max_element(tumorPcap.begin(), tumorPcap.end());
    Cell2D::setTumorProliferationMax(_tumorProliferationMax);
    std::cout << " ### _tumorProliferationMax = " << _tumorProliferationMax << std::endl;
    for (const unsigned i : range(TUcells.size()))
    {
        const unsigned index = (int)TUcells[i] -1;
        const unsigned row = index / _gridSize.y;
        const unsigned col = index - (_gridSize.x * row);
        _inputTumorCells.push_back(make_shared<Cell2D>(Cell2D::createTumorCell({row, col}, tumorPcap[i], isStem[i])));
    };
    std::cout << " ### Initialized \'"<< "TUcells" << "\' with " << _inputTumorCells.size() << " cells." << std::endl;

    if (true)
    {
        std::cout << std::endl;
        const json IM = MySystem["IM"];
        const json IMcells = IM["IMcells"];
        const json IMprop = IM["IMprop"];
        const json immunePcap = IMprop["Pcap"];
        const json Kcap = IMprop["Kcap"];
        const json engaged = IMprop["engaged"];

        // TODO: This is causing problems again, as needs to be remembered to be called.
        //       And is called the same, should be in one place.
        _immuneProliferationMax = *std::max_element(immunePcap.begin(), immunePcap.end());
        _immuneKillMax = *std::max_element(Kcap.begin(), Kcap.end());
        std::cout << " ### _immuneKillMax = " << _immuneKillMax << std::endl;
        Cell2D::setImmuneProliferationMax(_immuneProliferationMax);
        Cell2D::setImmuneKillingCapacityMax(_immuneKillMax);
        std::cout << " ### _immuneProliferationMax = " << _tumorProliferationMax << std::endl;

        for (const unsigned i : range(IMcells.size()))
        {
            const unsigned index = (int)IMcells[i] -1;
            const unsigned row = index / _gridSize.y;
            const unsigned col = index - (_gridSize.x * row);
            _inputImmuneCells.push_back(make_shared<Cell2D>(Cell2D::createImmuneCell({row, col}, immunePcap[i], Kcap[i])));
        };
        std::cout << " ### Initialized \'"<< "IMcells" << "\' with " << _inputImmuneCells.size() << " cells." << std::endl;
    }

    _isInitialized = true;
}

CellSystemEvolver::~CellSystemEvolver()
{
}

bool CellSystemEvolver::isInitialized() const
{
    return _isInitialized;
}

void CellSystemEvolver::setCellDiedSpontaneously(SharedCell cell, CellOccupationGrid& occupationGrid)
{
    assert(cell);

    Vertex2D pos = cell->getPosition();
    if (occupationGrid[(int)round(pos.y)][(int)round(pos.x)] != cell)
    {
        cell->dump();
        occupationGrid[(int)round(pos.y)][(int)round(pos.x)]->dump();
    }
    assert(occupationGrid[(int)round(pos.y)][(int)round(pos.x)] == cell);

    // Free current spot, update CellPointers after all looping is done.
    occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = nullptr;
    cell->setDiedSpontaneously();
}

void CellSystemEvolver::setCellDiedExhaustedTryingToProliferate(SharedCell cell, CellOccupationGrid& occupationGrid)
{
    Vertex2D pos = cell->getPosition();
    if (occupationGrid[(int)round(pos.y)][(int)round(pos.x)] != cell)
    {
        cell->dump();
        occupationGrid[(int)round(pos.y)][(int)round(pos.x)]->dump();
    }
    assert(occupationGrid[(int)round(pos.y)][(int)round(pos.x)] == cell);

    // Free current spot, update the vector<Cell2D> after all looping is done.
    occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = nullptr;
    cell->setDiedExhausted();
    ++immuneProliferatingExhausted;
}

std::vector<Index2D> CellSystemEvolver::getCellSurrounding(const SharedCell cell,
                                                           const CellOccupationGrid& occupationGrid,
                                                           const bool isSearchingFreeSpots)
{
    vector<Index2D> indices;
    for (const auto& relative : surroundingHelper)
    {
        const Index2D index = Index2D((int)round(cell->getPosition().x), (int)round(cell->getPosition().y)) + relative;
        if (index.x >=0 && (unsigned)index.x < occupationGrid[0].size() &&
            index.y >=0 && (unsigned)index.y < occupationGrid.size())
        {
            const SharedCell cell = occupationGrid[index.y][index.x];
            if ((isSearchingFreeSpots && !cell) || (!isSearchingFreeSpots && cell))
            {
                indices.push_back(index);
            }
        }
    }
    return indices;
}

void CellSystemEvolver::tumorGoGrowDie(CellPointers& tumorCells,
                                       CellOccupationGrid& occupationGrid)
{
    Chronograph chronograph(__FUNCTION__);

    // TODO!!!: IMPORTANT!!! All list erasures need to be performed this way!
    auto tumorIt = tumorCells.begin();
    while (tumorIt != tumorCells.end())
    {
        SharedCell cell = *tumorIt;
        assert(cell->isTumor() && cell->isAlive());

        const float maxRandomFloat = (_tumorProliferationProbability +
                                      _tumorDeathProbability +
                                      _tumorMigrationProbability);
        const float randomFloat = Random::real(maxRandomFloat);

        // Create surroundingIndexes if within the grid and check for free spots
        const std::vector<Index2D> freeSpots = getCellSurrounding(cell, occupationGrid, /*isSearchingFreeSpots*/ true);

        // TODO: These conditions are not very nice! Check sum(P) <= 1.0 etc.
        const bool isProliferating = freeSpots.size() > 0 && randomFloat < _tumorProliferationProbability;
        if (isProliferating)
        {
            if (cell->isExhausted())
            {
                setCellDiedExhaustedTryingToProliferate(cell, occupationGrid);
                tumorCells.erase(tumorIt++);
                tumorProliferatingExhausted++;
                continue;
            }

            // Add new cell.
            const Index2D freeSpotCoordinates = freeSpots[Random::integer(freeSpots.size() -1)];

            const bool isProliferatingSymmetric = cell->isStem() &&
                    Random::real() < _tumorSymmetricDivisionProbability;

            const SharedCell newCell = cell->proliferate(freeSpotCoordinates, isProliferatingSymmetric);
            // Update cell gridOccupation table.
            occupationGrid[freeSpotCoordinates.y][freeSpotCoordinates.x] = newCell;
            tumorCells.push_back(newCell);
            debugSuspiciousProliferationCapacity(tumorCells, newCell, occupationGrid);
            tumorProliferatingNumber++;
            ++tumorIt;
            continue;
        }
        else if (randomFloat >= _tumorProliferationProbability &&
                 randomFloat < _tumorProliferationProbability + _tumorDeathProbability)
        {
            setCellDiedSpontaneously(cell, occupationGrid);
            tumorCells.erase(tumorIt++);
            tumorDiedSpontaneouslyNumber++;
            continue;
        }
        else if (freeSpots.size() > 0 &&
                 randomFloat >= _tumorProliferationProbability + _tumorDeathProbability &&
                 randomFloat < _tumorProliferationProbability + _tumorDeathProbability + _tumorMigrationProbability)
        {
            // Move cell.
            const Index2D freeSpotCoordinates = freeSpots[Random::integer(freeSpots.size() -1)];
            const Vertex2D pos = cell->getPosition();
            // Free current spot.
            occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = nullptr;
            // Update position
            cell->setPosition(freeSpotCoordinates);
            // Occupy the new spot.
            occupationGrid[freeSpotCoordinates.y][freeSpotCoordinates.x] = cell;
            ++tumorMovingNumber;
            ++tumorIt;
            continue;
        }
    }
}

void CellSystemEvolver::immuneGo(CellPointers& immuneCells, CellOccupationGrid& occupationGrid, const vil_image_view<float>& chemotaxialGrid)
{
    Chronograph chronograph(__FUNCTION__);

    for (SharedCell& immuneCell : immuneCells)
    {
        chronograph.start("Get cell free spots");

        // Failing these assertions would mean deeper problems in previous step stages.
        assert(immuneCell->isImmune());
        assert(immuneCell->isAlive());

        // Create surroundingIndexes if within the grid and check for free spots
        const std::vector<Index2D> freeSpots = getCellSurrounding(immuneCell, occupationGrid, /*isSearchingFreeSpots*/ true);
        const unsigned freeSpotNumber = freeSpots.size();

        chronograph.log();
        chronograph.start("Get chemotaxis at free spots");
        if (freeSpotNumber <= 0)
            continue;

        std::vector<float> chemotaxyAtFreeSpots(freeSpotNumber);
        for (unsigned i = 0; i < freeSpotNumber; ++i)
        {
            const Index2D freeSpotCoordinates = freeSpots[i];
            chemotaxyAtFreeSpots[i] = getVilGridValue<float>(chemotaxialGrid, freeSpotCoordinates.x, freeSpotCoordinates.y);
        }

        chronograph.log();
        chronograph.start("Calculate min chemotaxis");
        const float maxChemotaxy = *max_element(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end());
        std::for_each(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end(),
                      [this, maxChemotaxy](float& chemotaxy) {
                            chemotaxy = (1.0 - _immuneRandomWalkFactor)*chemotaxy/maxChemotaxy * Random::real();
                       });
        const auto minChemotaxyIterator = min_element(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end());
        const unsigned minChemotaxyIndex = std::distance(chemotaxyAtFreeSpots.begin(), minChemotaxyIterator);
        chronograph.log();
        chronograph.start("Perform movement");

        // TODO: This seems like good for encapsulation as well - the order may be important here.
        // Move cell.
        const Index2D oldPos = immuneCell->getPosition();
        immuneCell->setPosition(freeSpots[minChemotaxyIndex]);
        occupationGrid[immuneCell->getPosition().y][immuneCell->getPosition().x] = immuneCell;
        // Free current spot.
        occupationGrid[(int)round(oldPos.y)][(int)round(oldPos.x)] = nullptr;
        chronograph.log();

        ++immuneAveageMovingSpeed;
    }

    chronograph.setLoggingEnabled(true);
}


void CellSystemEvolver::immuneGoGrowDie(CellPointers& immuneCells, CellOccupationGrid& occupationGrid, const vil_image_view<float>& chemotaxialGrid)
{// TODO: IMPORTANT! This function is almost the same as tumorGoGrowDie(), this probably requires only a simple refactoring.
    Chronograph chronograph(__FUNCTION__);

    for (auto cellsIt = immuneCells.begin(); cellsIt != immuneCells.end(); ++cellsIt)
    {
        chronograph.setLoggingEnabled(false);
        SharedCell immuneCell = *cellsIt;
        assert(immuneCell->isImmune() && immuneCell->isAlive());

        const float maxRandomFloat = (_immuneProliferationProbability +
                                      _immuneDeathProbability +
                                      _immuneMigrationProbability);
        const float randomFloat = Random::real(maxRandomFloat);

        // Create surroundingIndexes if within the grid and check for free spots
        const std::vector<Index2D> freeSpots = getCellSurrounding(immuneCell, occupationGrid, /*isSearchingFreeSpots*/ true);

        const unsigned freeSpotsNumber = freeSpots.size();

        // TODO: These conditions are not very nice! Check sum(P) <= 1.0 etc.
        const bool isProliferating = freeSpotsNumber > 0 && randomFloat < _immuneProliferationProbability;
        if (isProliferating)
        {
            if (immuneCell->isExhausted())
            {
                setCellDiedExhaustedTryingToProliferate(immuneCell, occupationGrid);
                immuneCells.erase(cellsIt);
                continue;
            }

            // Add new cell.
            const Index2D freeSpotCoordinates = freeSpots[Random::integer(freeSpotsNumber -1)];

            // TODO: encapsulate this block
            {
                const SharedCell newCell = immuneCell->proliferate(freeSpotCoordinates);
                // Update cell gridOccupation table.
                occupationGrid[freeSpotCoordinates.y][freeSpotCoordinates.x] = newCell;
                immuneCells.push_back(newCell);
                debugSuspiciousProliferationCapacity(immuneCells, newCell, occupationGrid);
                immuneProliferatingNumber++;
            }
        }
        else if (randomFloat >= _immuneProliferationProbability &&
                 randomFloat <  _immuneProliferationProbability +
                                _immuneDeathProbability)
        {
            setCellDiedSpontaneously(immuneCell, occupationGrid);
            immuneCells.erase(cellsIt);
            immuneDiedSpontaneouslyNumber++;
        }
        else if (freeSpots.size() > 0 &&
                 randomFloat >= _immuneProliferationProbability +
                                _immuneDeathProbability &&
                 randomFloat <  _immuneProliferationProbability +
                                _immuneDeathProbability +
                                _immuneMigrationProbability)
        {
            // Move cell.

            chronograph.log();
            chronograph.start("Get chemotaxis at free spots");

            std::vector<float> chemotaxyAtFreeSpots(freeSpots.size());
            for (unsigned i = 0; i < freeSpotsNumber; ++i)
            {
                const Index2D freeSpotCoordinates = freeSpots[i];
                chemotaxyAtFreeSpots[i] = getVilGridValue<float>(chemotaxialGrid, freeSpotCoordinates.x, freeSpotCoordinates.y);
            }

            chronograph.log();
            chronograph.start("Calculate min chemotaxis");
            const float maxChemotaxy = *max_element(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end());
            std::for_each(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end(),
                          [this, maxChemotaxy](float& chemotaxy) {
                                chemotaxy = (1.0 - _immuneRandomWalkFactor)*chemotaxy/maxChemotaxy * Random::real();
                           });
            const auto minChemotaxyIterator = min_element(chemotaxyAtFreeSpots.begin(), chemotaxyAtFreeSpots.end());
            const unsigned minChemotaxyIndex = std::distance(chemotaxyAtFreeSpots.begin(), minChemotaxyIterator);
            chronograph.log();
            chronograph.start("Perform movement");

            // TODO: This seems like good for encapsulation as well - the order may be important here.
            // Move cell.
            const Index2D oldPos = immuneCell->getPosition();
            immuneCell->setPosition(freeSpots[minChemotaxyIndex]);
            occupationGrid[immuneCell->getPosition().y][immuneCell->getPosition().x] = immuneCell;
            // Free current spot.
            occupationGrid[(int)round(oldPos.y)][(int)round(oldPos.x)] = nullptr;
            chronograph.log();

            ++immuneAveageMovingSpeed;
        }
        chronograph.setLoggingEnabled(true);
    }
}

void CellSystemEvolver::immuneKill(CellPointers& immuneCells,
                                   CellPointers& tumorCells,
                                   CellOccupationGrid& occupationGrid,
                                   const vil_image_view<float>& chemotaxialGrid)
{
    Chronograph chronograph(__FUNCTION__, false);

    bool wasKillingPerfomed = false;
    for (SharedCell& cell : immuneCells)
    {
        assert(cell->isImmune());

        if (!cell->hasKillingCapacity())
            continue;

        if (cell->isEngagedAfterKilling())
        {
            cell->subtractFromKillingEngagementDuration();
            continue;
        }

        if (Random::real() > _immuneKillingProbability)
            continue;

        const Vertex2D& pos = cell->getPosition();
        float distToTumor = getVilGridValue<float>(chemotaxialGrid, pos.x, pos.y);
        // Attack only closest neighbour tumor, and not over diagonals.
        if (distToTumor > 1.0f)
            continue;

        // Create surroundingIndexes if within the grid and check for spots with tumor.
        // TODO: reuse getCellSurrounding() here.
        vector<Index2D> tumorNeighbourIndexes;
        for (const auto& relative : surroundingHelper)
        {
            Index2D index = Index2D((int)round(pos.x), (int)round(pos.y)) + relative;
            if (index.x >= 0 && (unsigned)index.x < _gridSize.x &&
                index.y >= 0 && (unsigned)index.y < _gridSize.y)
            {
                const SharedCell neighgborCell = occupationGrid[index.y][index.x];
                if (neighgborCell != nullptr && neighgborCell->isTumor())
                {
                    tumorNeighbourIndexes.push_back(index);
                }
            }
        }

        // Comment from original Matlab version:
        // % find indices to killed cell and killer cell. If the unlikely case
        // % happens that one tumor cell is simultaneously killed by two immune cells,
        // % then both will be exhausted

        const unsigned tumorSpotsNumber = tumorNeighbourIndexes.size();
        // Although distToTumor <= 1.0 and there had to be some tumor cells in neighborhood
        // they already may have been killed.
        if (tumorSpotsNumber == 0)
            continue;

        const Index2D attackedPos = tumorNeighbourIndexes[Random::integer(tumorSpotsNumber -1)];

        SharedCell attackedTumorCell = occupationGrid[attackedPos.y][attackedPos.x];
        assert(attackedTumorCell->isTumor());
        occupationGrid[attackedPos.y][attackedPos.x]->setKilled();
        // Exhaust the killing cell.
        cell->setDidKilling();
        ++immuneAttackingNumber;
        wasKillingPerfomed = true;
    }

    if (wasKillingPerfomed)
    {// Cleanup of killed tumor cells.
     // TODO: Implement occupationGrid as weak pointers, not to have to do this cleanup maybe.
        for (auto tumorIt = tumorCells.begin(); tumorIt != tumorCells.end(); ++tumorIt)
        {
            SharedCell tumorCell = *tumorIt;
            assert(tumorCell->isTumor());
            const Index2D tumorPos = tumorCell->getPosition();
            if (occupationGrid[tumorPos.y][tumorPos.x]->wasKilled())
            {// If the tumor has been killed remove it from both containers.
                occupationGrid[tumorPos.y][tumorPos.x] = nullptr;
                ++tumorKilledNumber;
                tumorCells.erase(tumorIt);
            }
        }
    }
}

CellOccupationGrid CellSystemEvolver::createClearOccupationGrid(const Index2D& gridSize)
{
    Chronograph chrono(__FUNCTION__);

    assert(gridSize.x > 0);
    assert(gridSize.y > 0);

    CellOccupationGrid gridOccupation;
    gridOccupation.resize(gridSize.y);
    for(auto& row : gridOccupation)
    {
        row.resize(gridSize.x, SharedCell());
    }
    return gridOccupation;
}

vil_image_view<bool> CellSystemEvolver::prepareOccupiedVilGrid(const CellPointers& cells, const Index2D& gridSize)
{
    Chronograph chrono(__FUNCTION__);
    vil_image_view<bool> vilGrid(gridSize.y, gridSize.x);
    vilGrid.fill(false);
    for (const SharedCell& cell : cells)
    {
        const Vertex2D& pos = cell->getPosition();
        setVilGridValue<bool>(vilGrid, (int)round(pos.x), (int)round(pos.y), true);
    }
    return vilGrid;
}

void CellSystemEvolver::appendNewCells(CellPointers& cells,
                                       const CellPointers& newCells)
{
    Chronograph chrono(__FUNCTION__);

    const unsigned newCellsSize = newCells.size();
    if (newCellsSize == 0)
        return;

    // TODO: Check how to use merge().
    for (const SharedCell& cell : newCells)
    {
        cells.push_back(cell);
    }
}

CellOccupationGrid CellSystemEvolver::prepareOccupationGrid(const std::vector<const CellPointers*> cellSystems,
                                                            const Index2D& gridSize,
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                                                            CellPointers& tumorCellsToRemove,
#endif
                                                            unsigned& occupationAdded)
{
    Chronograph chrono(__FUNCTION__);
    // TODO: Replace that with a 2D array, e.g. https://codereview.stackexchange.com/questions/215399/2d-counterpart-of-stdarray-in-c17
    CellOccupationGrid occupationGrid = createClearOccupationGrid(gridSize);
    // Set grid cells to occupied.
    for (const CellPointers* cellSystem : cellSystems)
    {

        for (const SharedCell& cell : *cellSystem)
        {
            const Vertex2D& pos = cell->getPosition();

            SharedCell& cellAlreadyAtPosition = occupationGrid[(int)round(pos.y)][(int)round(pos.x)];
            if (cellAlreadyAtPosition)
            {
                // This condition means, that at least two cell systems overlap the occupation.
                // It is only possible with tumor/immune cells moving into permeabilized stroma.

                const bool isOneOfCellsImmune = cell->isImmune() || cellAlreadyAtPosition->isImmune();
                const bool isOneOfCellsTumor = cell->isTumor() || cellAlreadyAtPosition->isTumor();
                const bool isOneOfCellsFibrosis = cell->isFibrosis() || cellAlreadyAtPosition->isFibrosis();

                const bool isSuchSituationAllowed = isOneOfCellsFibrosis && (isOneOfCellsImmune || isOneOfCellsTumor);

                // Failure here would mean that two tumor cells, two immune cells,
                // or one immune and one tumor occupy the same position, which is prohibited.
                if (!isSuchSituationAllowed)
                {
                    cellAlreadyAtPosition->dump();
                    cell->dump();
#ifndef DEBUG_REPEATING_OCCUPATION_INDEXES
                    assert(isSuchSituationAllowed);
#endif
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                    // Remove tumor and replace it with immune.
                    if (cell->isImmune())
                    {
                        tumorCellsToRemove.push_back(occupationGrid[(int)round(pos.y)][(int)round(pos.x)]);
                        occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = cell;
                    }
                    else
                    {
                        tumorCellsToRemove.push_back(cell);
                    }

                    continue;
#endif
                }

                // Otherwise, the one, that is occupying now should be tumor or immune.
                occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = cell->isFibrosis() ? cellAlreadyAtPosition : cell;
            }
            else
            {
                occupationGrid[(int)round(pos.y)][(int)round(pos.x)] = cell;
                ++occupationAdded;
            }
        }
    }
    return occupationGrid;
}

vil_image_view<float> CellSystemEvolver::prepareChemotaxialGrid(const CellPointers& tumorCells)
{
    Chronograph chrono(__FUNCTION__);

    // There won't be further point than this one:
    const float verydistantValue = _gridSize.x + _gridSize.y;
    vil_image_view<float> chemotaxialGrid(_gridSize.x, _gridSize.y);
    chemotaxialGrid.fill(verydistantValue);
    // Set grid cells to occupied.
    uint numberOfTumorCells = 0;
    for (const SharedCell& cell : tumorCells)
    {
        assert(cell->isTumor());

        const Vertex2D& pos = cell->getPosition();
        setVilGridValue<float>(chemotaxialGrid, pos.x, pos.y, 0);
        ++numberOfTumorCells;
    }

    if (numberOfTumorCells > 0)
    {
        vil_distance_transform(chemotaxialGrid);
    }

    return chemotaxialGrid;
}

vil_image_view<float> CellSystemEvolver::prepareHypoxiaGrid(const std::vector<const CellPointers*> cellSystems)
{
    Chronograph chrono(__FUNCTION__);

    // These are NOTES from hypoxia map from the original code.
//    % HypoxMap is essentially a distance map for the distance from the tumor edge
//    % within the tumor. All values larger than distMaxNecr are cut at this
//    % threshold. Then, the mask is normalized to 0 ... 1 with 1 being distMaxNecr
//    defRadius = 4;
//    mySystem.params.fillSE = strel('disk',defRadius); % smoothing structure for hypoxia map

//    HypoxMap = min(double(bwdist(~imdilate(Lt|Ln|Lf,fillSE),'euclidean')),distMaxNecr) / distMaxNecr;
//        HypoxMap = min(double(bwdist(~imdilate(Lt|Ln|Lf,fillSE),'euclidean')),distMaxNecr) / distMaxNecr;

    vil_image_view<bool> occupation(_gridSize.x, _gridSize.y);
    vil_image_view<bool> occupationDilated;
    vil_image_view<float> hypoxiaGrid;

    occupation.fill(0.0f);
    // Set grid cells occupied by all cell systems.
    uint numberOfOccupiedCells = 0;
    for (const CellPointers* cellSystem: cellSystems)
    {
        for (const SharedCell& cell : *cellSystem)
        {
            const Vertex2D& pos = cell->getPosition();
            setVilGridValue<bool>(occupation, pos.x, pos.y, true);
            ++numberOfOccupiedCells;
        }
    }

    hypoxiaGrid.set_size(_gridSize.x, _gridSize.y);
    if (numberOfOccupiedCells > 0)
    {
        vil_binary_dilate(occupation, occupationDilated, necrosisSeedDisk);

        assert(occupation.ni() == occupationDilated.ni());
        assert(occupation.nj() == occupationDilated.nj());

        // In our case, we dont invert image, as 0 values in vil_distance_transform()
        // represent outside of tumor at this point. Let's only fill non-zeros with "very distant value".
        const float veryDistantValue = _gridSize.x + _gridSize.y;
        for (unsigned j = 0; j < occupationDilated.nj(); ++j)
        {
            for (unsigned i = 0; i < occupationDilated.ni(); ++i)
            {
                setVilGridValue<float>(hypoxiaGrid, i, j, veryDistantValue*getVilGridValue<bool>(occupationDilated, i, j));
            }
        }

        // Find distance to tumor edge.
        vil_distance_transform(hypoxiaGrid);

        for (unsigned j = 0; j < hypoxiaGrid.nj(); ++j)
        {
            for (unsigned i = 0; i < hypoxiaGrid.ni(); ++i)
            {
                const float distanceToTumorEdge = getVilGridValue<float>(hypoxiaGrid, i, j);
                setVilGridValue<float>(hypoxiaGrid, i, j, distanceToTumorEdge > _necrosisDistMax ? 1.0f : distanceToTumorEdge/_necrosisDistMax);
            }
        }
    }
    else
    {
        hypoxiaGrid.fill(1.0);
    }

#ifdef DEBUG_DUMP_HYPOXIA_GRID
    FILE* file = fopen("hypoxiagrid.txt","w");
    for (int j = 0; j < hypoxiaGrid.nj(); ++j)
    {
        for (int i = 0; i < hypoxiaGrid.ni(); ++i)
        {
            const float value = *(hypoxiaGrid.begin()+i*hypoxiaGrid.istep() + j*hypoxiaGrid.jstep());
            if (value > 0.0)
            {
                std::fprintf(file, "%3.2f ", value);
            }
            else
            {
                std::fprintf(file, "%-4s ", "  O ");
            }
        }
        std::fprintf(file, "\n");
    }
    fclose(file);
#endif //DEBUG_DUMP_HYPOXIA_GRID

    return hypoxiaGrid;
}

CellPointers CellSystemEvolver::preparePermutedCells(const CellPointers& systemState)
{
    Chronograph chrono(__FUNCTION__);

    // TODO: Use std::random_shuffle here and there.
    CellPointers systemPermutedState = randomPermutation(systemState);
    assert(systemPermutedState.size() == systemState.size());
    return systemPermutedState;
}

unsigned CellSystemEvolver::immuneAppendNewRandomized(CellOccupationGrid& occupationGrid,
                                                      CellPointers& immuneCellSystem,
                                                      unsigned freeSpotsNumber)
{
    Chronograph chrono(__FUNCTION__);
    unsigned newImmuneCellsAdded = 0u;

    unsigned newImmuneCellsLeft = _immuneFlowRate;
    assert(occupationGrid.size() > 0 && occupationGrid[0].size() > 0);
    const Index2D gridSize = {occupationGrid[0].size(), occupationGrid.size()};

    while (freeSpotsNumber > 0 && newImmuneCellsLeft > 0)
    {// TODO: think of optimizing this. E.g. - start directly
     //       from the pointed position and go in both directions? - rather not.
     //       Maybe have the occupied indexes always sorted and just jump directly.

        // Get all free spots first.
        // TODO: calculate this only once, will be a bottleneck if immuneFlowRate > 1.
        std::vector<Index2D> freeSpots;
        for (unsigned row = 0; row < gridSize.y; ++row)
        {
            for(unsigned col = 0; col < gridSize.x; ++col)
            {
                SharedCell cell = occupationGrid[row][col];
                if (!cell)
                {
                    freeSpots.push_back({col, row});
                    continue;
                }
                // TODO: Move this debug-check to a separate function.
                assert(cell->getPosition().x == col && cell->getPosition().y == row);
            }
        }
        // TODO: Move this debug-check to a separate function.
        assert(freeSpots.size() == freeSpotsNumber);

        const Index2D emptyCoords = freeSpots[Random::integer(freeSpotsNumber -1)];
        assert(!occupationGrid[emptyCoords.y][emptyCoords.x]);

        // Add new immune cell and update the grid.
        immuneCellSystem.push_back(make_shared<Cell2D>(Cell2D::createImmuneCell(emptyCoords, _immuneProliferationMax, _immuneKillMax)));
        occupationGrid[emptyCoords.y][emptyCoords.x] = immuneCellSystem.back();

        --newImmuneCellsLeft;
        --freeSpotsNumber;
        ++newImmuneCellsAdded;
    }
    return newImmuneCellsAdded;
}

CellPointers CellSystemEvolver::permeabilizedStromaSubset(const CellPointers& fibrosisCells)
{
    CellPointers fibrosisSubset;
    for (const SharedCell& cell : fibrosisCells)
    {
        if (Random::real() > _fibrosisStromaPermeability)
        {
            fibrosisSubset.push_back(cell);
        }
    }
    return fibrosisSubset;
}

// TODO: It would be nice to keep input parameters in the same order for all functions.
void CellSystemEvolver::fibrosisSeeding(CellPointers& immuneCells, CellPointers& fibrosisCells,
                                        const CellPointers& tumorCells, const CellPointers& necrosisCells)
{
    Chronograph chronograph(__FUNCTION__, true);

    // Collect all immune cells prone to fibrosis.
    vil_image_view<bool> fibrosisSeedMap;
    {// Wrap it up in brackets to get rid of the structure initialized here.
        CellPointers potentiallyFibrosifyingCells;
        for (const SharedCell& cell : immuneCells)
        {
            assert(cell);
            assert(cell->isImmune());

            if (cell->hasKillingCapacity() || Random::real() > _fibrosisProbSeed)
            {
                continue;
            }
            potentiallyFibrosifyingCells.push_back(cell);
        }

        if (potentiallyFibrosifyingCells.size() == 0)
            return;

        fibrosisSeedMap = prepareOccupiedVilGrid(potentiallyFibrosifyingCells, _gridSize);
    }


    //TODO : This part is identical as in ID: aslkdjalksdjlkdjas
    vil_image_view<bool> dilatedFibrosisSeedMap;
    const vil_structuring_element fibrosisDilationElement =
            prepareStructuringElement(fibrosisGrowthDisk, _fibrosisFraction);
    vil_binary_dilate(fibrosisSeedMap, dilatedFibrosisSeedMap, fibrosisDilationElement);

    // Reject the spots with tumor from fibrosifiction.
    for (const SharedCell& tumorCell : tumorCells)
    {
        assert(tumorCell->isTumor());
        const Index2D pos = tumorCell->getPosition();

        const bool isOccupiedByTumor = getVilGridValue<bool>(dilatedFibrosisSeedMap, pos.x, pos.y);
        if (!isOccupiedByTumor)
            continue;

        setVilGridValue<bool>(dilatedFibrosisSeedMap, pos.x, pos.y, false);
    }

    // TODO: this loop is almost the same, as the previous.
    // Reject the spots with necrosis from fibrosifiction.
    for (const SharedCell& necrosisCell : necrosisCells)
    {
        assert(necrosisCell->isNecrosis());
        const Index2D pos = necrosisCell->getPosition();

        const bool isOccupiedByNecrosis = getVilGridValue<bool>(dilatedFibrosisSeedMap, pos.x, pos.y);
        if (!isOccupiedByNecrosis)
            continue;

        setVilGridValue<bool>(dilatedFibrosisSeedMap, pos.x, pos.y, false);
    }

    CellPointers fibrosifyingCells;
    auto immuneIt = immuneCells.begin();
    while (immuneIt != immuneCells.end())
    {
        SharedCell& immuneCell = *immuneIt;
        assert(immuneCell);
        assert(immuneCell->isImmune());
        const Index2D pos = immuneCell->getPosition();

        if (!getVilGridValue<bool>(dilatedFibrosisSeedMap, pos.x, pos.y))
        {
            ++immuneIt;
            continue;
        }

        // Finally, FIBROSIFY!
        immuneCells.erase(immuneIt++);

        SharedCell newFibrosisCell = make_shared<Cell2D>();
        *newFibrosisCell = Cell2D::createFibrosisCell(pos);
        fibrosisCells.push_back(newFibrosisCell);
        ++immuneTurnedToFibrosis;
    }
}

// TODO: Refactor this, so it is more general.
// TODO: Then - test it, to make sure, that it is statistically correct.
CellPointers CellSystemEvolver::randomWeightedSampleWithReplacement(const CellPointers& cells,
                                                                    const unsigned size,
                                                                    const vil_image_view<float>& weights)
{
    unsigned copySize = size;
    std::vector<float> hypoxyValuesForTumorCells(cells.size());
    //TODO: Rather use std::accumulate here.
    auto tumorIt = cells.begin();
    float sumaricalHypoxies = 0;
    for (unsigned i = 0; i < cells.size(); ++i, ++tumorIt)
    {
        const Index2D pos = (*tumorIt)->getPosition();
        const float hypoxy = getVilGridValue<float>(weights, pos.x, pos.y);
        hypoxyValuesForTumorCells[i] = hypoxy;
        sumaricalHypoxies += hypoxy;
    }

    CellPointers sample;
    while (copySize--)
    {
        const float randomFloat = Random::real(sumaricalHypoxies);
        float sum = 0;
        tumorIt = cells.begin();
        for (unsigned i = 0; i < hypoxyValuesForTumorCells.size(); ++i, ++tumorIt)
        {
            sum += hypoxyValuesForTumorCells[i];
            if (sum < randomFloat)
                continue;

            sample.push_back(*tumorIt);
            break;
        }
    }

    return sample;
}

vil_structuring_element CellSystemEvolver::prepareStructuringElement(const vil_structuring_element& element,
                                                                     const float seedingFraction) const
{
    Chronograph chronograph(__FUNCTION__, true);
    // TODO: Make sure the structuring element is initialized.
    static const int min_i = element.min_i();
    static const int min_j = element.min_j();
    static const unsigned width = 1 + element.max_i() - min_i;
    static const unsigned height = 1 + element.max_j() - min_j;
    static const unsigned noOfPoints = element.p_i().size();

    // Initialize structuring element to close the randomized seed neighborhood.
    static  bool isImageClosingStructuringElementInitialized = false;
    static vil_structuring_element imageClosingStructuringElement;
    if (!isImageClosingStructuringElementInitialized)
    {

        // TODO: This magic number has no explanation in original code as well.
        constexpr unsigned size = 5;
        constexpr unsigned halfSize = size/2;

        std::vector<int> p_i;
        std::vector<int> p_j;

        for (int i = -halfSize; i < (int)(halfSize + 1); ++i)
        {
            for (int j = -halfSize; j < (int)(halfSize + 1); ++j)
            {
                p_i.push_back(i);
                p_j.push_back(j);
            }
        }
        imageClosingStructuringElement.set(p_i, p_j);
        isImageClosingStructuringElementInitialized = true;
    }


    // Transform structuring element to an image, to be able to apply binary image close to it.
    vil_image_view<bool> seedNeighbours(width, height);
    seedNeighbours.fill(false);
    vil_image_view<bool> seedNeighboursOut(width, height);
    for (unsigned i = 0; i < noOfPoints; ++i)
    {
        if (Random::real() > seedingFraction)
            continue;

        setVilGridValue<bool>(seedNeighbours,
                              element.p_i()[i]-min_i,
                              element.p_j()[i]-min_j, true);
    }
    vil_binary_closing(seedNeighbours, seedNeighboursOut, imageClosingStructuringElement);

    // Finaly transform the closed image back to a structuring element for necrosis seedmap dilation.
    const unsigned size_i = seedNeighboursOut.ni();
    const unsigned size_j = seedNeighboursOut.nj();
    std::vector<int> p_i;
    std::vector<int> p_j;
    for (int i = 0; i < (int)size_i; ++i)
    {
        for (int j = 0; j < (int)size_j; ++j)
        {
            if (!getVilGridValue<bool>(seedNeighboursOut, i, j))
                continue;

            p_i.push_back(i + min_i);
            p_j.push_back(j + min_j);
        }
    }
#ifdef DEBUG_DUMP_STRUCTURING_ELEMENT
    seedNeighbours.fill(false);

    FILE* file = fopen("structuring.txt","w");
    for (int j = 0; j < width; ++j)
    {
        for (int i = 0; i < height; ++i)
        {
            const bool value = getVilGridValue<bool>(seedNeighboursOut, i, j);
//            std::fprintf(file, "%-4s ", value ? "X" : "O");
            std::printf("%-1s ", value ? "X" : "O");
        }
//        std::fprintf(file, "\n");
        std::printf("\n");
    }
    fclose(file);
//    assert(false);
#endif //DEBUG_DUMP_STRUCTURING_ELEMENT

    return vil_structuring_element(p_i, p_j);
}

void CellSystemEvolver::necrosisSeeding(CellPointers& tumorCells, const vil_image_view<float> hypoxiaGrid)
{
    Chronograph chronograph(__FUNCTION__);

    unsigned newNecrosisSeeds = 0;
    for (unsigned i = 0; i < tumorCells.size(); ++i)
    {
        newNecrosisSeeds += (unsigned)(Random::real() <= _necrosisProbSeed);
    }

    if (tumorCells.size() > 1 && newNecrosisSeeds > 0)
    {
        // TODO: Should probably sample without replacement - look at original code notes.
        //       But with replacement is easier, quicker and probability of sampling the same
        //       tumor cell is rather low.
        CellPointers tumorSeedingNecrosis =
                randomWeightedSampleWithReplacement(tumorCells, newNecrosisSeeds, hypoxiaGrid);
        const vil_image_view<bool> necrosisSeedMap =
                prepareOccupiedVilGrid(tumorSeedingNecrosis, _gridSize);

//        if (isSpacePressed)
//        {
//            if (necrosisSeedMap.size() < 1)
//            {
//                std::cout << " NO OCCUPATION " << std::endl;
//            }

//            for (unsigned i = 0; i < necrosisSeedMap.ni(); ++i)
//            {
//                for (unsigned j = 0; j < necrosisSeedMap.nj(); ++j)
//                {
//                    std::printf("%s", getVilGridValue<bool>(necrosisSeedMap, i, j) ? "X" : "O");
//                }
//                std::printf("\n");
//            }

//            std::fflush(stdout);
//        }

        //TODO : This part is identical as in ID: aslkdjalksdjlkdjas
        vil_image_view<bool> dilatedNecrosisSeedMap;
        const vil_structuring_element necrosisDilationElement = prepareStructuringElement(necrosisGrowthDisk, _necrosisFraction);
        vil_binary_dilate(necrosisSeedMap, dilatedNecrosisSeedMap, necrosisDilationElement);

        // TODO!!!: IMPORTANT!!! All list erasures need to be performed this way!
        auto tumorIt = tumorCells.begin();
        while(tumorIt != tumorCells.end())
        {
            assert(*tumorIt);

            const Index2D pos = (*tumorIt)->getPosition();

            const bool isCellNecrosified = getVilGridValue<bool>(dilatedNecrosisSeedMap, pos.x, pos.y);
            if (!isCellNecrosified)
            {
                ++tumorIt;
                continue;
            }

            // Tumor cell dies and new necrosis cell is created.
//            setCellDiedTurnedToNecrosis(*tumorIt, occupationGrid);
            tumorIt = tumorCells.erase(tumorIt++);
            tumorDiedTurnedToNecrosis++;

            SharedCell newNecrosisCell = std::make_shared<Cell2D>();
            *newNecrosisCell = Cell2D::createNecrosisCell(pos);
            assert(newNecrosisCell->isNecrosis());
            _inputNecrosisCells.push_back(newNecrosisCell);
        }
    }
}

unsigned CellSystemEvolver::nextStep(bool isSpacePressed)
{
    if (!_isInitialized)
    {
        std::cout << __FUNCTION__ << " WARNING: System is not initalized. Nothing to do. Quitting. " << std::endl;
        return 0;
    }

    // TODO: Go through all the questions in the original code, write them down and discuss with L&O.
    Chronograph chronograph(__FUNCTION__, true);


    tumorDiedSpontaneouslyNumber = 0u;
    tumorProliferatingNumber = 0u;
    tumorMovingNumber = 0u;
    tumorProliferatingExhausted = 0u;
    tumorKilledNumber = 0u;
    tumorDiedTurnedToNecrosis = 0u;

    immuneNewNumber = 0u;
    immuneAttackingNumber = 0u;
    immuneProliferatingNumber = 0u;
    immuneProliferatingExhausted = 0u;
    immuneEngagedNumber = 0u;
    immuneDiedSpontaneouslyNumber = 0u;
    immuneAveageMovingSpeed = 0.0f;
    immuneTurnedToFibrosis = 0u;

    // Prepare a subset of the _inputFibrosisCells, which is permeable,
    // and thus - looks like unoccupied for tumor.
    CellPointers fibrosisCellsSubset = permeabilizedStromaSubset(_inputFibrosisCells);
    unsigned totalOccupation = 0;

#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
    CellPointers tumorCellsToRemove;
#endif
    CellOccupationGrid occupationGrid =
            prepareOccupationGrid({&_inputTumorCells,
                                   &_inputImmuneCells,
                                   &fibrosisCellsSubset,
                                   &_inputNecrosisCells}, _gridSize,
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                                   tumorCellsToRemove,
#endif
                                   totalOccupation);
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
    std::cout << " ### _inputTumorCells has size of: " << _inputTumorCells.size() << std::endl;
    std::cout << " ### tumorCellsToRemove has size of: " << tumorCellsToRemove.size() << std::endl;
    _inputTumorCells.remove_if([tumorCellsToRemove](const SharedCell& cell) {
//        std::cout << " ### tumorCellsToRemove, removing: " << *std::find(tumorCellsToRemove.begin(), tumorCellsToRemove.end(), cell) << std::endl;
        return tumorCellsToRemove.end() != std::find(tumorCellsToRemove.begin(), tumorCellsToRemove.end(), cell);
    });
    std::cout << " ### AFTER _inputTumorCells  has size of: " << _inputTumorCells .size() << std::endl;
    tumorCellsToRemove.clear();
#endif

    // There can never be more cells, than indices.
    uint32 totalSystemSize = _inputTumorCells.size() + _inputImmuneCells.size();
    const Index2D gridSize = {occupationGrid[0].size(), occupationGrid.size()};
    const unsigned totalGridIndexNumber = gridSize.x * gridSize.y;
    assert(totalSystemSize <= totalGridIndexNumber);

    CellPointers tumorCells = preparePermutedCells(_inputTumorCells);

    // Tumor cells' round.
    const unsigned tumorCellSizeBeforeTumorRound = tumorCells.size();
    tumorGoGrowDie(tumorCells, occupationGrid);
    // Assert, that cell balance is kept correctly here.
    const int tumorCellBalance = tumorProliferatingNumber - tumorDiedSpontaneouslyNumber - tumorProliferatingExhausted;
    assert(tumorCellSizeBeforeTumorRound + tumorCellBalance == tumorCells.size());
    totalOccupation += tumorCellBalance;

    // There can never be more cells, than indices.
    // TODO: Make sure this is the case with fibrosis and necrosis as well.
    //      E.g. whether fibrosis can occupy the same grid spot ad immune cell.
    totalSystemSize = tumorCells.size() + _inputImmuneCells.size();
    assert(totalSystemSize <= totalGridIndexNumber);

    // Immune cells' round.
    CellPointers immuneCells = _inputImmuneCells;
    if (_immuneFlowRate > 0 && _immuneFlowProb >= Random::real())
    {
        const unsigned freeSpotsNumber = _gridSize.x*_gridSize.y - totalOccupation;
        immuneNewNumber = immuneAppendNewRandomized(occupationGrid, immuneCells, freeSpotsNumber);
    }

    // This needs to be done after tumor moves, proliferates and dies.
    // TODO: If so, encapsulate this in system state class as well.
    vil_image_view<float> chemotaxialGrid = prepareChemotaxialGrid(tumorCells);
    vil_image_view<float> hypoxiaGrid = prepareHypoxiaGrid({&tumorCells, &_inputNecrosisCells, &_inputFibrosisCells});

    // Let the immune cells move.
    chronograph.start("immuneGoKillDisengage-wholeLoop");
    // Prepare a subset of the _inputFibrosisCells, which is permeable,
    // and thus - looks like unoccupied for immune cells.
    fibrosisCellsSubset = permeabilizedStromaSubset(_inputFibrosisCells);
    if (fibrosisCellsSubset.size() > 0)
    {// Update occupation grid, because fibrosis is permeabilized differently for immune, than for tumor cells.
        //TODO: This could be refactored and sped up if necessary.
        totalOccupation = 0;
        occupationGrid = prepareOccupationGrid({&tumorCells,
                                                &immuneCells,
                                                &fibrosisCellsSubset,
                                                &_inputNecrosisCells}, _gridSize,
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                                                tumorCellsToRemove,
#endif
                                                totalOccupation);
    }

    // TODO: Shuffling here is done only once, and later all cells move in the same order.
    immuneCells = preparePermutedCells(immuneCells);
    for (unsigned i = 0; i < _immuneSpeedMax; ++i)
    {
        // TODO: This is costly, work it around? Needs to be discussed, but it seems, that statistically
        //      precalculating some random movement along the chemotaxial map instead of repeating this in
        //      every step could speed up calculations very significantly. It would, however, lower precision of
        //      system behavior representation.
        immuneGo(immuneCells, occupationGrid, chemotaxialGrid);
        immuneKill(immuneCells, tumorCells, occupationGrid, chemotaxialGrid);

        // TODO: Could hold local reference to engaged cells' table for performance,
        //      but profiling shows, it would give us only give us a few % speed up here...
        for (SharedCell& immuneCell : immuneCells)
        {
            if (!immuneCell->isEngagedAfterKilling())
                continue;
            immuneCell->subtractFromKillingEngagementDuration();
        }
    }
    // Allow immune cells to move once more or to proliferate or die.
    // TODO: Look at the comment in the original MATLAB code with my modifications.
    immuneGoGrowDie(immuneCells, occupationGrid, chemotaxialGrid);
    immuneAveageMovingSpeed  /= immuneCells.size();
    chronograph.log("immuneGo-wholeLoop");

    // Necrosis seeding round.
    // TODO!: IMPORTANT! Update occupation grid inside this function, not to have to calculate it afterwards.
    necrosisSeeding(tumorCells, hypoxiaGrid);

    // Fibrosis seeding round.
    fibrosisSeeding(immuneCells, _inputFibrosisCells, tumorCells, _inputNecrosisCells);


    //Some debug-logging
    std::cout << " ################## Number of agents:    " << tumorCells.size() + immuneCells.size() + _inputNecrosisCells.size() + _inputFibrosisCells.size()<< " at " << ++_steps << " steps." << std::endl
              << " # Tumor agents:                         " << tumorCells.size()            << std::endl
              << " #    Tumor moving:                      " << tumorMovingNumber            << std::endl
              << " #    Tumor proliferating:               " << tumorProliferatingNumber     << std::endl
              << " #    Tumor died spontaneously:          " << tumorDiedSpontaneouslyNumber << std::endl
              << " #    Tumor proliferating but exhausted: " << tumorProliferatingExhausted  << std::endl
              << " #    Tumor killed:                      " << tumorKilledNumber            << std::endl
              << " #    Tumor died as necrosis:            " << tumorDiedTurnedToNecrosis    << std::endl
              << " # Immune agents:                        " << immuneCells.size()           << std::endl
              << " #    Immune new cells:                  " << immuneNewNumber              << std::endl
              << " #    Immune attacking:                  " << immuneAttackingNumber        << std::endl
              << " #    Immune proliferating:              " << immuneProliferatingNumber    << std::endl
              << " #    Immune proliferating exhausted:    " << immuneProliferatingExhausted << std::endl
              << " #    Immune average moving speed:       " << immuneAveageMovingSpeed      << std::endl
              << " #    Immune engaged:                    " << immuneEngagedNumber          << std::endl
              << " #    Immune fibrosified:                " << immuneTurnedToFibrosis       << std::endl
              << " # Necrosis agents:                      " << _inputNecrosisCells.size()   << std::endl
              << " # Fibrosis agents:                      " << _inputFibrosisCells.size()   << std::endl
              << " ####################################    " << std::endl;

//    dumpSystem(permutedSystem);
//    calculateDumpGridOccupation(permutedSystem);

    _inputTumorCells.swap(tumorCells);
    _inputImmuneCells.swap(immuneCells);

    return _steps;
}

std::vector<Cell2D> CellSystemEvolver::getContiguousCellSystemArray()
{
    const unsigned systemSize = _inputTumorCells.size() + _inputImmuneCells.size() + _inputFibrosisCells.size() + _inputNecrosisCells.size();
    std::vector<Cell2D> system(systemSize);
    unsigned i = 0;
    //FIBROSIS/NECROSIS go first, as they may be permeable and covered by the other.
    for (const SharedCell& cell : _inputFibrosisCells)
    {
        system[i++] = *cell;
    }
    for (const SharedCell& cell : _inputNecrosisCells)
    {
        system[i++] = *cell;
    }
    for (const SharedCell& cell :_inputTumorCells)
    {
        system[i++] = *cell;
    }
    for (const SharedCell& cell :_inputImmuneCells)
    {
        system[i++] = *cell;
    }
    return system;

}

bool CellSystemEvolver::isTumorDead() const
{
    return _inputTumorCells.size() == 0;
}

Index2D CellSystemEvolver::getGridSize() const
{
    return _gridSize;
}

void CellSystemEvolver::calculateDumpGridOccupation(std::vector<const CellPointers*> systemState, const Index2D& gridSize)
{
    CellOccupationGrid gridOccupation = createClearOccupationGrid(gridSize);
    unsigned totalOccupation = 0;
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
    CellPointers tumorCellsToRemove;
#endif
    gridOccupation = prepareOccupationGrid(systemState, gridSize,
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                                           tumorCellsToRemove,
#endif
                                           totalOccupation);

    if (gridOccupation.size() < 1)
    {
        std::cout << " NO OCCUPATION " << std::endl;
    }

    for (unsigned j = 0; j < gridOccupation.size(); ++j)
    {
        for (unsigned i = 0; i < gridOccupation[0].size(); ++i)
        {
            std::printf("%7p ", (void*)(gridOccupation[j][i]).get());
        }
        std::printf("\n");
    }

    std::fflush(stdout);
}

void CellSystemEvolver::dumpSystem(const CellPointers& systemState)
{
    for(const SharedCell& sharedCell : systemState)
    {
        sharedCell->dump();
    }
}
