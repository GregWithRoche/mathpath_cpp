#pragma once

#include <string>

using namespace std;

struct ShaderSrc
{
    static const string vertex;
    static const string geometryPointToSquare;
    static const string fragment;
};
