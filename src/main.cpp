/*
 * main.cpp
 *
 *  Created on: Sep 16, 2020
 *      Author: Grzegorz Ilnicki
 */

// Link statically, don't define this:
// #define GLFW_DLL
#include <cstdio>
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <set>
#include <algorithm>
#include <limits>
#include <ctime>
#include <chrono>

#include <glm/glm.hpp>

#include "ShaderSrc.h"
#include "GLWrapper.h"
#include "CellSystemEvolver.h"
#include "Chronograph.h"

using namespace glm;

// TODO: IMPORTANT With grid size 500x250, it seems to have crashed in the end (tumor not killed).
static Index2D gridSize = {330, 330};
static const unsigned MAX_STEPS = 60;
static unsigned WINDOW_SIDE_X = gridSize.x*(1000/gridSize.y);
static unsigned WINDOW_SIDE_Y = gridSize.y*(1000/gridSize.y);

static bool isSimulationPaused = false;
static bool isSpacePressed = false;
static bool isRPressed = false;

// TODO: use the following code inside matlab to dump the desired (here: "MySystem") structure to a json file.
// jsonStr = jsonencode(MySystem);
// fid = fopen('C:\Users\ilnickig\projects\MATLAB\MySystemJson.txt', 'w');
// if fid == -1, error('Cannot create JSON file'); end
// fwrite(fid, jsonStr, 'char');
// fclose(fid);
static const std::string inputSystemJsonFilePath("C:/Users/ilnickig/projects/MATLAB/MySystemJson.txt");
//static const std::string inputSystemJsonFilePath("C:/Users/ilnickig/projects/MATLAB/MySystemJson_mod.txt");

static void key_callback( GLFWwindow* window, int key, int scancode, int action, int mods )
{
    if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS )
        glfwSetWindowShouldClose ( window, GLFW_TRUE );

    if ( key == GLFW_KEY_P && action == GLFW_PRESS )
    {
        isSimulationPaused = !isSimulationPaused;
    }

    if ( key == GLFW_KEY_SPACE && action == GLFW_PRESS )
    {
        isSpacePressed = !isSpacePressed;
    }

    if ( key == GLFW_KEY_R && action == GLFW_PRESS )
    {// Restart simulation.
        isRPressed = !isRPressed;
    }
}

int main(void)
{
    chrono::milliseconds msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());

    GLFWwindow* window;
//     if (!GLWrapper::initWindow(window))
//         return -1;
    
    /* Initialize the library */
    if (!glfwInit())
        return -1;
    
    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    

    CellSystemEvolver evolver;
    const bool isStartingSystemFromFile = true;
    if (isStartingSystemFromFile)
    {
        evolver.initializeFromFile(inputSystemJsonFilePath);
    }
    else
    {
        // Setup initial system.
        // Tumor max proliferations setup should go first here, before creating a cell.
        // TODO: If it is necessary to remember this, think how to solve it.
        static const int tumorMaxDivisions = 10;
        Cell2D::setTumorProliferationMax(tumorMaxDivisions);
        evolver.initializeInPlace({Cell2D::createTumorCell({gridSize.x/2, gridSize.y/2}, tumorMaxDivisions, true)}, gridSize);
    }


    if (!evolver.isInitialized())
    {
        std::cout << " ### ERROR MathPath_cpp: Evolver not initialized correctly. QUITTING." << std::endl;
        return 0;
    }

    // TODO: Wrapping this will not work, why?
    gridSize = evolver.getGridSize();
    WINDOW_SIDE_X = gridSize.x*(1000/gridSize.y);
    WINDOW_SIDE_Y = gridSize.y*(1000/gridSize.y);
    window = glfwCreateWindow(WINDOW_SIDE_X, WINDOW_SIDE_Y, "MathPath_cpp", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    // Make the window's context current.
    glfwMakeContextCurrent(window);
    // Set a keypress callback.
    glfwSetKeyCallback(window, key_callback);

    if (!gladLoadGL(glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }
    glfwSwapInterval(1);

    std::cout << " ### Hello MathPath_cpp! All systems working." << std::endl;
    
    GLuint shaderProgramGLID = GLWrapper::createShaderProgram();
    
    // A VBO GL ID - will store vertices along with colors:
    GLuint vertexBufferObjectGLID; 
    // Generate a vertex buffer object with that ID:
    glGenBuffers(1, &vertexBufferObjectGLID);
    // Activate the vertex buffer object:
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectGLID);
    // Assign the vertices data to the vertex buffer object:
    vector<Cell2D> cellSystem = evolver.getContiguousCellSystemArray();
    glBufferData(GL_ARRAY_BUFFER, cellSystem.size()*sizeof(Cell2D), cellSystem.data(), GL_DYNAMIC_DRAW);

    // Vertex Array Object stores all the links between the attributes and Vertex Buffer Object.
    GLuint vertexArrayObjectGLID;
    glGenVertexArrays(1, &vertexArrayObjectGLID);
    glBindVertexArray(vertexArrayObjectGLID);
    
    // Create parameter/data locations in shader.
    GLint mvpLocationGLID = glGetUniformLocation(shaderProgramGLID, "MVP");
    GLint vertexInputColorLocationGLID = glGetAttribLocation( shaderProgramGLID, "vertexInputColor");
    GLint vertexInputPositionLocationGLID = glGetAttribLocation( shaderProgramGLID, "vertexInputPosition");
    
    
    // Element Buffer Object - stores the order for the vertices in the VBO.
//     GLuint elementBufferObjectGLID;
//     glGenBuffers(1, &elementBufferObjectGLID);
//     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObjectGLID);
//     glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(squareVertexIDs), squareVertexIDs, GL_STATIC_DRAW);
    
    // Specify, how to retrieve data for this input from the array.
    glVertexAttribPointer(/* reference to the input*/ vertexInputPositionLocationGLID,
                          /* number of coords*/ 2,
                          /* the type of the coord*/ GL_FLOAT,
                          /* whether to normalize */ GL_FALSE,
                          /* the stride between vertex definitions */ sizeof(Cell2D),
                          /* the offset in Vertex to the parameter */ (void*) Cell2D::positionOffset());
    glEnableVertexAttribArray(vertexInputPositionLocationGLID);
    
    // Here analogically for the color attribute.
    glVertexAttribPointer(/* reference to the input*/ vertexInputColorLocationGLID, 
                          /* number of coords*/ 3, 
                          /* the type of the coord*/ GL_FLOAT, 
                          /* whether to normalize */ GL_FALSE,
                          /* the stride between vertex definitions */ sizeof(Cell2D), 
                          /* the offset in Vertex to the parameter */ (void*) Cell2D::colorOffset());
    glEnableVertexAttribArray(vertexInputColorLocationGLID);
    
    Chronograph chronograph("MAIN_GL_LOOP");
    // TODO: Get exact time in milliseconds here, to verify it with the chronograph.
    unsigned steps = 0;
    while (!glfwWindowShouldClose(window))
    {
        chronograph.setLoggingEnabled(!isSimulationPaused && !evolver.isTumorDead());
        chronograph.start("GL_RENDER_ACTIONS");
        float ratio;
        constexpr float marginRatio = 0.0;
        int width, height;

        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        
        chronograph.start("Evolution");
        if (!isSimulationPaused && !evolver.isTumorDead())
        {
            steps = evolver.nextStep(isSpacePressed);
        }
        chronograph.log("Evolution");

        // Reload buffer data
        cellSystem = evolver.getContiguousCellSystemArray();
        glBufferData(GL_ARRAY_BUFFER, cellSystem.size()*sizeof(Cell2D), cellSystem.data(), GL_DYNAMIC_DRAW);
    
        glm::mat4x4 projectionMatrix = glm::ortho(-marginRatio*(gridSize.x), (1.0f + marginRatio)*(gridSize.x),
                                                  -marginRatio*(gridSize.y), (1.0f + marginRatio)*(gridSize.y),1.0f, -1.0f);
        glm::mat4x4 translationMatrix = translate(identity<mat4x4>(), {0.5f, 0.5f, 0.0f});
        glm::mat4x4 mvp = projectionMatrix*translationMatrix;
        glUniformMatrix4fv(mvpLocationGLID, 1, GL_FALSE, (const GLfloat*) &mvp);
        
        // Clear the screen
        constexpr float greyShade = 0.15f;
        glClearColor(greyShade, greyShade, greyShade, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Render frame
        glDrawArrays(GL_POINTS, 0, evolver.getContiguousCellSystemArray().size());

        glfwSwapBuffers(window);
        glfwPollEvents();

        chronograph.log();

        static bool isTumorAdvertisedDead = false;
        if (!isTumorAdvertisedDead && evolver.isTumorDead())
        {
            std::cout << " ######################################################" << std::endl
                      << " ### THE END: The tumor is dead. ######################" << std::endl
                      << " ######################################################" << std::endl;

            chrono::milliseconds msTotal = chrono::duration_cast< chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()) - msStart;
            unsigned long long ms =     msTotal.count() % 1000;
            unsigned long long sec =  ((msTotal.count() - ms) / 1000) % 60;
            unsigned long long min = (((msTotal.count() - ms) / 1000) - sec) / 60;

            std::cout << " ########################################################" << std::endl
                      << " ### Time elapse sinced start to tumor death [min:sec:ms]: [" << min << ":" << sec << ":" << ms << "]" << std::endl
                      << " ########################################################" << std::endl;

            isTumorAdvertisedDead = true;
        }

        chronograph.setLoggingEnabled(isSimulationPaused);
        if (steps == MAX_STEPS)
            break;
    }

    chrono::milliseconds msTotal = chrono::duration_cast< chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()) - msStart;
    unsigned long long ms =     msTotal.count() % 1000;
    unsigned long long sec =  ((msTotal.count() - ms) / 1000) % 60;
    unsigned long long min = (((msTotal.count() - ms) / 1000) - sec) / 60;

    std::cout << " ######################################################" << std::endl
              << " ### Time elapse sinced start total [min:sec:ms]: [" << min << ":" << sec << ":" << ms << "]" << std::endl
              << " ######################################################" << std::endl;

    glfwTerminate();  
    return 0;
}
