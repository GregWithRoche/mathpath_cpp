#pragma once

#include <chrono>
#include <cstdio>
#include <iostream>
#include <string>
#include <list>
#include <utility>
#include <assert.h>


struct Chronograph
{
    using rep = unsigned long long;
    using period =  std::ratio<1, 1750000000u>; // My machine is 1.6-1.9 GHz

    // Define real time units
    using picoseconds = std::chrono::duration<unsigned long long, std::pico>;
    using nanoseconds = std::chrono::nanoseconds;
    using microseconds = std::chrono::microseconds;
    using milliseconds =  std::chrono::milliseconds;
    using duration = std::chrono::duration<rep, period>;
    using Cycle = std::chrono::duration<double, period>;
    // Define double-based unit of clock tick
    using TimePoint = std::chrono::time_point<Chronograph>;
    using ActionTime = std::pair<std::string, TimePoint>;
    static const bool isSteady = true;

    ~Chronograph()
    {// This will allow not to call log, when geetting out of a function.
        log();
    }

    Chronograph(const std::string&& measuredTimeAction, const bool isLoggingEnabled = false) : _isLoggingEnabled(isLoggingEnabled)
    {
        start(measuredTimeAction);
    }

    void setLoggingEnabled(bool isEnabled)
    {
        _isLoggingEnabled = isEnabled;
    }

    static TimePoint now() noexcept
    {
        unsigned lo, hi;
        asm volatile("rdtsc" : "=a" (lo), "=d" (hi));
        return TimePoint(duration(static_cast<rep>(hi) << 32 | lo));
    }
    
    void start(const std::string& measuredTimeAction)
    {
        if (!_isLoggingEnabled)
            return;

        _measuredTimeActions.push_back({measuredTimeAction, Chronograph::now()});
        std::cout << " ### Chronograph::start() for action: " << measuredTimeAction << std::endl;
    }
    
    void log(const std::string& measuredTimeAction = std::string())
    {
        if (!_isLoggingEnabled || _measuredTimeActions.size() == 0)
            return;

        const TimePoint t1 = Chronograph::now();
        TimePoint t0;
        std::string measuredTimeActionFetched;

        if (measuredTimeAction.empty())
        {// Get the last action time point.
            measuredTimeActionFetched.swap(_measuredTimeActions.back().first);
            t0 = _measuredTimeActions.back().second;
            _measuredTimeActions.pop_back();
        }
        else
        {// Look for the last matching string (serving as action "bracket" marker).
            auto revIt = _measuredTimeActions.rbegin();
            for ( ; revIt != _measuredTimeActions.rend(); ++revIt)
            {
                if ((*revIt).first == measuredTimeAction)
                {
                    measuredTimeActionFetched.swap((*revIt).first);
                    t0 = (*revIt).second;
                    break;
                }
            }

            // If no such, ignore.
            if (revIt == _measuredTimeActions.rend())
                return;

            _measuredTimeActions.erase(std::next(revIt).base());
        }

        // Get the clock ticks since restart for given action.
        auto ticks = Chronograph::Cycle(t1-t0);
        std::cout << " # Chronoraph::log(), time for:         " << measuredTimeActionFetched << " # milliseconds/kiloCPUticks: "
                  << std::chrono::duration_cast<milliseconds>(ticks).count() << " / " << ticks.count()/1000
                  << std::endl;
    }

private:
    bool _isLoggingEnabled = false;
    std::list<ActionTime> _measuredTimeActions;
};
