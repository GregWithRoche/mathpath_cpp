#include "Cell2d.h"
#include <cstdio>
#include <iostream>
#include <memory>

unsigned Cell2D::_tumorProliferationMax = 1u;
unsigned Cell2D::_immuneProliferationMax = 1u;
unsigned Cell2D::_immuneKillingCapacityMax = 1u;
unsigned Cell2D::_totalKillingEngagementDuration = 0u;

static constexpr Color tumorBaseColor   (1,   0, 0);
static constexpr Color immuneBaseColor  (0,   0, 1);
static constexpr Color necrosisBaseColor(0, 0.2, 0);
static constexpr Color fibrosisBaseColor(0.75,   0.60, 0);

static constexpr int uninitializedProliferationCapacity = -1000;

// TODO: IMPORTANT: make sure all members are always somehow initialized.
Cell2D::Cell2D() :
    _color({1, 1, 1}),
    _type(Type::Unknown),
    _proliferationCapacity(uninitializedProliferationCapacity),
    _killingEngagementDurationLeft(0)
{
}

Cell2D::Cell2D(Cell2D::Type type) :
    _type(type),
    _proliferationCapacity(uninitializedProliferationCapacity),
    _killingEngagementDurationLeft(0)
{
    if (type == Type::Immune)
    {
        _color = immuneBaseColor;
        _killingCapacity = Cell2D::_immuneKillingCapacityMax;
        return;
    }

    _killingCapacity = 0;

    if (type == Type::Tumor)
    {
        _color = tumorBaseColor;
        return;
    }
    else if (type == Type::Necrosis)
    {
        _color = necrosisBaseColor;
        return;
    }
    else if (type == Type::Fibrosis)
    {
        _color = fibrosisBaseColor;
        return;
    }

    // We should never get here.
    assert(false);
}

void Cell2D::setPosition(const Vertex2D &pos)
{
    _position = pos;
}

int Cell2D::getProliferationCapacity() const
{
    return _proliferationCapacity;
}

void Cell2D::updateColorIntensity()
{
    // Necrosis and fibrosis don't change color intensity.
    assert(isImmune() || isTumor());

    // Use some residual color to better visually recognize the cell type in the end of its proliferation/killing.
    const float residualIntensity = isTumor() ? 28.0f/255.0f : 90.0f/255.0f;
    const float divisionPart = isTumor() ? (float) Cell2D::_tumorProliferationMax :
                                           (float) Cell2D::_immuneKillingCapacityMax;

    const float divisorPart = (isTumor() ? _proliferationCapacity : _killingCapacity);


    const float intensity = residualIntensity + ( (1.0f - residualIntensity) * (divisorPart >= 0 ? divisorPart : 0) ) / divisionPart;
    if (intensity > 1.0 || intensity <= 0)
    {
        std::cout << " residualIntensity " << residualIntensity << std::endl;
        std::cout << " divisionPart " << divisionPart << std::endl;
        std::cout << " divisorPart " << divisorPart << std::endl;
        std::cout << " intensity " << intensity << std::endl;
    }
    assert(intensity <= 1.0);
    assert(intensity > 0.0);

    _color = (isTumor() ? tumorBaseColor : immuneBaseColor)  * intensity;
}

void Cell2D::setProliferationCapacity(int capacity)
{
    // Necrosis and fibrosis don't proliferate...
    assert(isImmune() || isTumor());

    if (_proliferationCapacity == capacity)
        return;

    _proliferationCapacity = capacity;
    if (!isTumor())
        return;

    updateColorIntensity();
}

void Cell2D::setIsStem(bool isStem)
{
    _isStem = isStem;
}

SharedCell Cell2D::proliferate(const Vertex2D& freeSpotCoordinates, const bool isProliferatingSymmetric)
{
    // Necrosis and fibrosis don't proliferate...
    assert(isImmune() || isTumor());

    // Immune cells don't proliferate symmetrically.
    assert(!isProliferatingSymmetric || !isImmune());
    // If not symmetric division, subtract from proliferationCapacity.
    setProliferationCapacity(_proliferationCapacity - (isProliferatingSymmetric ? 0 : 1));
    SharedCell newCell = std::make_shared<Cell2D>();
    // TODO: IMPORTANT! For immune cells, killing capacity may not be copied - check it.
    *newCell = isTumor() ? Cell2D::createTumorCell(freeSpotCoordinates, _proliferationCapacity, isProliferatingSymmetric) :
                           Cell2D::createImmuneCell(freeSpotCoordinates, _proliferationCapacity, _immuneKillingCapacityMax);
    return newCell;
}

bool Cell2D::isTumor() const
{
    return _type == Type::Tumor;
}

bool Cell2D::isImmune() const
{
    return _type == Type::Immune;
}

bool Cell2D::isNecrosis() const
{
    return _type == Type::Necrosis;
}

bool Cell2D::isFibrosis() const
{
    return _type == Type::Fibrosis;
}

void Cell2D::setDidKilling()
{
    assert(isImmune());
    assert(hasKillingCapacity());
    assert(!isEngagedAfterKilling());

    --_killingCapacity;
    _killingEngagementDurationLeft = _totalKillingEngagementDuration;
    updateColorIntensity();
}

void Cell2D::setDiedSpontaneously()
{
    setProliferationCapacity(CellDeathReason::DiedSpontaneously);
}

bool Cell2D::diedSpontaneously() const
{
    return _proliferationCapacity == CellDeathReason::DiedSpontaneously;
}

void Cell2D::setKilled()
{
    assert(isTumor());
    setProliferationCapacity(CellDeathReason::WasKilled);
}

bool Cell2D::wasKilled() const
{
    return _proliferationCapacity == CellDeathReason::WasKilled;
}

void Cell2D::setDiedExhausted()
{
    setProliferationCapacity(CellDeathReason::DiedExhausted);
}

bool Cell2D::diedExhausted() const
{
    return _proliferationCapacity == CellDeathReason::DiedExhausted;
}

void Cell2D::setTumorProliferationMax(unsigned proliferationMax)
{
    _tumorProliferationMax = proliferationMax;
}

void Cell2D::setImmuneProliferationMax(unsigned proliferationMax)
{
    _immuneProliferationMax = proliferationMax;
}

bool Cell2D::isEngagedAfterKilling() const
{
    return (_killingEngagementDurationLeft > 0);
}

void Cell2D::subtractFromKillingEngagementDuration()
{
    assert(0 != _killingEngagementDurationLeft);
    --_killingEngagementDurationLeft;
}

const Color& Cell2D::getColor() const
{
    return _color;
}

size_t Cell2D::positionOffset()
{
    return offsetof(Cell2D, _position);
}

size_t Cell2D::colorOffset()
{
    return offsetof(Cell2D, _color);
}

int Cell2D::getTumorProliferationMax()
{
    return _tumorProliferationMax;
}

void Cell2D::setImmuneKillingEngagementDuration(const unsigned duration)
{
    _totalKillingEngagementDuration = duration;
}

void Cell2D::setImmuneKillingCapacityMax(const unsigned capacity)
{
    _immuneKillingCapacityMax = capacity;
}

void Cell2D::dump()
{
    std::cout << "Cell:" << this << " [ "
              << "_position(x,y=[" << getPosition().x << "," << getPosition().y << "], "
              << "_color(r,g,b)=[" << _color.r << "," << _color.g << "," << _color.b << "], "
              << "_isStem=[" << _isStem << "], "
              << "_type=[" << _type << "], "
              << "_proliferationCapacity=[" << _proliferationCapacity << "], "
              << "_killingCapacity=[" << _killingCapacity << "], "
              << "_isEngagedAttacking=[" << _killingEngagementDurationLeft << "]"
              << " ]" << std::endl;
}


Cell2D& Cell2D::operator=(const Cell2D& other)
{
    _position                         = other._position;
    _color                            = other._color;
    _type                             = other._type;
    _isStem                           = other._isStem;
    _proliferationCapacity            = other._proliferationCapacity;
    _killingCapacity                  = other._killingCapacity;
    _killingEngagementDurationLeft    = other._killingEngagementDurationLeft;

    return *this;
}

Cell2D Cell2D::createTumorCell(const Vertex2D& coordinates, const int proliferationCapacity, const bool isStem)
{

    Cell2D newTumorCell(Cell2D::Tumor);
    newTumorCell.setPosition(coordinates);
    newTumorCell.setProliferationCapacity(proliferationCapacity);
    newTumorCell.setIsStem(isStem);
    return newTumorCell;
}

Cell2D Cell2D::createImmuneCell(const Vertex2D& coordinates, const unsigned proliferationCapacity, const unsigned killingCapacity)
{
    Cell2D newImmuneCell(Cell2D::Immune);
    newImmuneCell.setPosition(coordinates);
    newImmuneCell.setProliferationCapacity(proliferationCapacity);
    newImmuneCell.setIsStem(false);
    newImmuneCell._killingCapacity = killingCapacity;
    return newImmuneCell;
}

Cell2D Cell2D::createNecrosisCell(const Vertex2D& coordinates)
{
    Cell2D newNecrosisCell(Cell2D::Necrosis);
    newNecrosisCell.setPosition(coordinates);
    newNecrosisCell.setIsStem(false);
    return newNecrosisCell;
}

Cell2D Cell2D::createFibrosisCell(const Vertex2D& coordinates)
{
    Cell2D newFibrosisCell(Cell2D::Fibrosis);
    newFibrosisCell.setPosition(coordinates);
    newFibrosisCell.setIsStem(false);
    return newFibrosisCell;
}
