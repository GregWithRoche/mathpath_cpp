#include "GLWrapper.h"
#include "ShaderSrc.h"

#include <cstdio>
#include <iostream>
#include <string>

using namespace glm;
using namespace std;

GLuint GLWrapper::compileShaderObject ( GLenum shaderTypeGLMacro, const GLchar* shaderCode )
{
    // A shader object ID - will store shader program(s).
    GLuint shaderID = glCreateShader ( shaderTypeGLMacro );
    // Set the source code for the vertex shader.
    // The last parameter can contain an array of source code string lengths.
    // Passing NULL simply makes it stop at the null terminator.
    glShaderSource ( shaderID, 1, &shaderCode, NULL );
    // Compile the shader code.
    glCompileShader ( shaderID );

    GLint shaderCompilationStatus;
    // Check shader compilation status and output.
    glGetShaderiv ( shaderID, GL_COMPILE_STATUS, &shaderCompilationStatus );
    char shaderCompilationOutputBuffer[512];
    glGetShaderInfoLog ( shaderID, 512, NULL, shaderCompilationOutputBuffer );
    if ( shaderCompilationStatus != GL_TRUE ) {
        std::cout << string ( shaderCompilationOutputBuffer ) << "\n";
    }

    return shaderID;
}


void GLWrapper::drawSquare ( const glm::mat4x4& projection, const glm::vec3&& translationVector, GLuint mvpLocationGLID, const glm::vec3& color, GLuint colorLocationGLID )
{
    mat4x4 translation = translate ( identity<mat4x4>(), translationVector );
    mat4x4 mvp = projection*translation;

    glUniformMatrix4fv ( mvpLocationGLID, 1, GL_FALSE, ( const GLfloat* ) &mvp );
    glUniform3fv ( colorLocationGLID, 1, ( const GLfloat* ) &color );
    glDrawElements ( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
}


int GLWrapper::initWindow(GLFWwindow* window )
{
    return 1;
}


GLuint GLWrapper::createShaderProgram()
{
    GLuint vertexShaderGLID =   GLWrapper::compileShaderObject ( GL_VERTEX_SHADER,   ShaderSrc::vertex.c_str());
    GLuint geometryShaderGLID = GLWrapper::compileShaderObject ( GL_GEOMETRY_SHADER, ShaderSrc::geometryPointToSquare.c_str());
    GLuint fragmentShaderGLID = GLWrapper::compileShaderObject ( GL_FRAGMENT_SHADER, ShaderSrc::fragment.c_str());

    GLuint shaderProgramGLID = glCreateProgram();
    glAttachShader ( shaderProgramGLID, vertexShaderGLID );
    glAttachShader ( shaderProgramGLID, geometryShaderGLID );
    glAttachShader ( shaderProgramGLID, fragmentShaderGLID );
    // This one is not necessary, as we have ony 1 color framebuffer at the moment
    // and it binds to the 0-th buffer by default. Use glDrawBuffers when rendering
    // to multiple framebuffers.
    glBindFragDataLocation ( shaderProgramGLID, 0, "fragmentOutputColor" );
    glLinkProgram ( shaderProgramGLID );
    glUseProgram ( shaderProgramGLID );
    
    return shaderProgramGLID;
}

