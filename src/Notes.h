#pragma once

static const short unsigned int gridSize = 585; // maximum 585*585*6 bytes == 2MB, a size of cache on HP 840 G6 running Windows

//set some grid cells to occupied
//    setGridValue(gridCells, 0, 0, 0);
//    setGridValue(gridCells, 9, 9, 0);
//    setGridValue(gridCells, 2, 5, 0);
//    setGridValue(gridCells, 7, 7, 0);
//    setGridValue(gridCells, 7, 6, 0);

//    for (int j = 0; j < gridCells.nj(); ++j)
//    {
//        for (int i = 0; i < gridCells.ni(); ++i)
//        {
//            std::printf("val(%d,%d)=%d\n", i, j, *(gridCells.begin()+i*gridCells.istep() + j*gridCells.jstep()));
//        }
//    }

//     Print before:
//    for (int j = 0; j < gridCells.nj(); ++j)
//    {
//        for (int i = 0; i < gridCells.ni(); ++i)
//        {
//            std::printf("%5.2f ", *(gridCells.begin()+i*gridCells.istep() + j*gridCells.jstep()));
//        }
//        std::printf("\n");
//    }


//     Print after:
//    for (int j = 0; j < gridCells.nj(); ++j)
//    {
//        for (int i = 0; i < gridCells.ni(); ++i)
//        {
//            std::printf("%5.2f ", *(gridCells.begin()+i*gridCells.istep() + j*gridCells.jstep()));
//        }
//        std::printf("\n");
//    }


// vector<Cell2D> cells = {
//     {{0, 0}, {1.0f, 0.0f, 0.0f}, tumorMaximumDivisions},
//     {{1, 0}, {1.0f, 1.0f, 0.0f}, tumorMaximumDivisions},
//     {{2, 0}, {0.0f, 1.0f, 0.0f}, tumorMaximumDivisions},
//     {{3, 0}, {0.0f, 1.0f, 0.0f}, tumorMaximumDivisions},
//     {{0, 1}, {1.0f, 0.0f, 1.0f}, tumorMaximumDivisions},
//     {{0, 2}, {0.0f, 0.0f, 1.0f}, tumorMaximumDivisions}
// };

/* needed for set insertion */
//bool Cell2DCompare(const Cell2D& one, const Cell2D& other)
//{
//    if (one.position.x < other.position.x) return true;
//    if (one.position.y < other.position.y) return true;

//    return false;
//};

// bool operator == (const Cell2D& one, const Cell2D& other)
// {
//     return one.position == other.position;
// }

// static const Cell2D colorVertices2D[] = {
//     {{0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}}
// };

// Definition of a 1x1 square.
// static const Vertex2D squareVertices[] = {
//     { -0.5f, -0.5f },
//     { -0.5f,  0.5f },
//     {  0.5f,  0.5f },
//     {  0.5f, -0.5f }
// };
// static GLuint squareVertexIDs[] = {0, 1, 2, 3, 0, 2};

