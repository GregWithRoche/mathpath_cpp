#pragma once

#include <stdlib.h>
#include <vector>
#include <list>
#include <cmath>
#include <ctime>
#include <algorithm>
#include <random>

#include <Chronograph.h>

using namespace std;

// Function to generate n non-repeating random elements
template <class T>
list<T> randomPermutation(const list<T>& l)
{
    list<T> out;

    // List permutation is SLOOOW, better do this on vector.
    const unsigned n = l.size();
    vector<T> permutedVector(n);
    auto listIt = l.begin();
    for (unsigned i = 0; i < n; ++i, ++listIt)
    {
        permutedVector[i] = (*listIt);
    }

    permutedVector = randomPermutation(permutedVector);
    for (unsigned i = 0; i < n; ++i)
    {
        out.push_back(permutedVector[i]);
    }
    return out;
}

  
// Function to return the next random number 
template <class T>
T getRandomVectorElement(vector<T>& v)
{ 
    Chronograph chronograph(__FUNCTION__, /*isLoggingEnabled*/ false);

    const int numberOfElements = v.size();
  
    // Generate a random number 
    // TODO: reuse the Random class.
    chronograph.start("Creating random device");
    std::random_device r;
    chronograph.log();
    chronograph.start("Creating random engine");
    std::default_random_engine generator(r());
    chronograph.log();
    chronograph.start("Creating random distribution");
    std::uniform_int_distribution<int> distribution(0, numberOfElements -1);
    chronograph.log();
    chronograph.start("Gettin a number from distribution");
    const int index = distribution(generator);
  
    // Get random element from the vector 
    T element = v[index]; 
  
    // Remove the number from the vector 
    swap(v[index], v[numberOfElements - 1]);
    v.pop_back(); 
  
    // Return the removed number 
    return element; 
} 
  
// Function to generate n non-repeating random elements
template <class T> 
vector<T> randomPermutation(const vector<T>& v) 
{ 
    
    int n = v.size();
    if (n <= 1)
        return v;

    vector<T> copy = v;
    vector<T> out;

    out.resize(n);
    
    // While vector has elements get a random element from it 
    // and assign to the other
    --n;
    while (n > 0)
    { 
        out[n--] = getRandomVectorElement(copy);
    }
    out[0] = copy[0];
    
    return out;
} 
