#include "ShaderSrc.h"

// Make sure that the output of the vertex shader and the input of the geometry/fragment shader 
// have the same name ("vertexOutputColor"), or the shaders will not be linked properly.

const string ShaderSrc::vertex = R"glsl(
    #version 150 core
    
    in vec3 vertexInputColor;
    in vec2 vertexInputPosition;
    
    out vec3 vertexOutputColor;

    void main()
    {
        gl_Position = vec4(vertexInputPosition, 0.0, 1.0);
        vertexOutputColor = vertexInputColor;
    }
)glsl";

const string ShaderSrc::geometryPointToSquare = R"glsl(
    #version 150 core
    
    uniform mat4 MVP;

    layout(points) in;
    layout(triangle_strip, max_vertices = 5) out;
    
    // Output from vertex shader for each vertex.    
    in vec3 vertexOutputColor[];
    
    // Output to fragment shader.
    out vec3 geometryOutputColor;

    void main()
    {
        geometryOutputColor = vertexOutputColor[0]; // Point (from 'layout') has only one vertex.
        
        const float halfWidth = 0.50f;
    
        gl_Position = MVP * (gl_in[0].gl_Position + vec4(-halfWidth, halfWidth, 0.0, 0.0));
        EmitVertex();

        gl_Position = MVP * (gl_in[0].gl_Position + vec4(halfWidth, halfWidth, 0.0, 0.0));
        EmitVertex();

        gl_Position = MVP * (gl_in[0].gl_Position + vec4(halfWidth, -halfWidth, 0.0, 0.0));
        EmitVertex();

        gl_Position = MVP * (gl_in[0].gl_Position + vec4(-halfWidth, -halfWidth, 0.0, 0.0));
        EmitVertex();

        gl_Position = MVP * (gl_in[0].gl_Position + vec4(-halfWidth, halfWidth, 0.0, 0.0));
        EmitVertex();

        EndPrimitive();
    }
)glsl";

const string ShaderSrc::fragment = R"glsl(
    #version 150 core

    in vec3 geometryOutputColor;
    out vec4 fragmentOutputColor;
    
    void main()
    {
        fragmentOutputColor = vec4(geometryOutputColor, 1.0);
    }
)glsl";

