#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <list>
#include <array>
#include <memory>
#include <random>

#include <Cell2d.h>

#define DEBUG_REPEATING_OCCUPATION_INDEXES

using Index2D = glm::ivec2;
using CellPointers = std::list<std::shared_ptr<Cell2D>>;
using CellPointerVector = std::vector<std::shared_ptr<Cell2D>>;
// TODO?: Move to using std::weak_ptr for that,
// this would free us from the burden of cleaning-up of the occupationGrid by hand.
using CellOccupationGrid = std::vector<CellPointerVector>;

template <class T>
class vil_image_view;
class vil_structuring_element;

// An interesing system behaviour right from the start.
static const int happySeed = 1601893583;
// std::shuffle(v.begin(), v.end(), std::mt19937{std::random_device{}()});
class Random
{
public:
    static float real(float maxReal = 1.0)
    {
        return std::uniform_real_distribution<float>(0.0f, maxReal)(generator);
    }
    static unsigned integer(unsigned maxUnsigned)
    {
        return std::uniform_int_distribution<unsigned>(0, maxUnsigned)(generator);
    }

private:
    static std::random_device r;
    static std::default_random_engine generator;
};

class CellSystemEvolver
{
public:
    CellSystemEvolver();
    CellSystemEvolver(const std::vector<Cell2D>& systemStateIn, const Index2D& gridSize);
    CellSystemEvolver(const std::string& systemJsonFilePath);
    ~CellSystemEvolver();

    bool isInitialized() const;
    void initializeInPlace(const std::vector<Cell2D>& systemStateIn, const Index2D& gridSize);
    void initializeFromFile(const std::string& systemJsonFilePath);

    unsigned nextStep(bool isSpacePressed = false);
    std::vector<Cell2D> getContiguousCellSystemArray();
    bool isTumorDead() const;
    Index2D getGridSize() const;

    // DEBUG helpers.
    static void dumpSystem(const CellPointers& systemState);
    static void calculateDumpGridOccupation(std::vector<const CellPointers*> systemState,
                                            const Index2D& gridSize);

private:

    // General functions.
    static CellOccupationGrid createClearOccupationGrid(const Index2D& gridSize);
    void setCellDiedSpontaneously(SharedCell cell, CellOccupationGrid& occupationGrid);
    void setCellDiedExhaustedTryingToProliferate(SharedCell cell, CellOccupationGrid& occupationGrid);
    void appendNewCells(CellPointers& cells, const CellPointers& newCells);
    static std::vector<Index2D> getCellSurrounding(const SharedCell cell,
                                                   const CellOccupationGrid& occupationGrid,
                                                   const bool isSearchingFreeSpots);

    // TODO: These may go together in one class:
    static CellOccupationGrid prepareOccupationGrid(const std::vector<const CellPointers*> cellSystems,
                                                    const Index2D& gridSize,
#ifdef DEBUG_REPEATING_OCCUPATION_INDEXES
                                                    CellPointers& tumorCellsToRemove,
#endif
                                                    unsigned& occupationAdded);

    CellPointers preparePermutedCells(const CellPointers& systemState);

    // VIL toolset related functions.
    void initializeVilStructuringElements();
    vil_image_view<bool> prepareOccupiedVilGrid(const CellPointers& systemPermutedState,
                                                const Index2D& gridSize);
    vil_image_view<float> prepareChemotaxialGrid(const CellPointers& systemAfterTumorRound);
    vil_image_view<float> prepareHypoxiaGrid(const std::vector<const CellPointers*> cellSystems);
    CellPointers randomWeightedSampleWithReplacement(const CellPointers& cells,
                                                     const unsigned size,
                                                     const vil_image_view<float>& weights);
    vil_structuring_element prepareStructuringElement(const vil_structuring_element& element,
                                                      const float seedingFraction) const;

    // Tumor functions.
    void tumorGoGrowDie(CellPointers& tumorCells, CellOccupationGrid& occupationGrid);

    // Immune functions.
    unsigned immuneAppendNewRandomized(CellOccupationGrid& occupationGrid,
                                       CellPointers& immuneCellSystem,
                                       unsigned freeSpotsNumber);
    void immuneGo(CellPointers& immuneCells,
                  CellOccupationGrid& occupationGrid,
                  const vil_image_view<float>& chemotaxialGrid);
    void immuneKill(CellPointers& immuneCells,
                    CellPointers& tumorCells,
                    CellOccupationGrid& occupationGrid,
                    const vil_image_view<float>& chemotaxialGrid);
    void immuneGoGrowDie(CellPointers& immuneCells,
                         CellOccupationGrid& occupationGrid,
                         const vil_image_view<float>& chemotaxialGrid);

    // Necrosis functions.
    // TODO: Make sure necrosis structuring element is working identically, as in original code.
    void necrosisSeeding(CellPointers& tumorCells, const vil_image_view<float> hypoxiaGrid);

    // Fibrosis functions
    CellPointers permeabilizedStromaSubset(const CellPointers& _inputFibrosisCells);
    void fibrosisSeeding(CellPointers& immuneCells, CellPointers& fibrosisCells,
                         const CellPointers& tumorCells, const CellPointers& necrosisCells);

    bool _isInitialized;
    Index2D _gridSize;
    unsigned _steps;

    // TODO: These may go together in one class with prepareOccupationGrid()
    CellPointers _inputTumorCells;
    CellPointers _inputImmuneCells;
    CellPointers _inputNecrosisCells;
    CellPointers _inputFibrosisCells;

    // Tumor cell parameters. 
    float _tumorSymmetricDivisionProbability;
    float _tumorProliferationProbability;
    unsigned _tumorProliferationMax;
    float _tumorMigrationProbability;
    float _tumorDeathProbability;

    // Immune cell parameters.
    float _immuneKillMax;                   // killing capacity of immune cells, default 5
    float _immuneProliferationMax;          // proliferation capacity of immune cells, default 10
    float _immuneProliferationProbability;  // HISTO GENERATED - probability of proliferation
    float _immuneMigrationProbability;      // probability of migrating, default 0.7
    float _immuneKillingProbability;        // probability of killing, default 0.1
    float _immuneDeathProbability;          // HISTO GENERATED - probability of spontaneous death
    float _immuneRandomWalkFactor;          // random influence on movement, default 0.75; GI: how is this established? doesn't seem well grounded
    float _immuneSpeedMax;                  // speed of immune cell movement, default 97
    float _immuneFlowProb;                  // probability of immune cell influx, def. 0.72
    unsigned _immuneFlowRate;               // how many immune cells appear simultaneously
    float _immuneKillingEngagementDuration; // how many intermed. steps is a killing cell engaged? default 48 (=6 hrs)

    // Necrosis parameters.
    unsigned _necrosisDistMax;              // if necrosis occurs, then it occurs within 2 mm (134 = approx. 2 mm)
    float _necrosisProbSeed;                // seed necrosis
    float _necrosisFraction;                // size of necrotic seed, 0...1, default 0.3

    // Fibrosis parameters.
    float _fibrosisProbSeed;                // probability of turning into fibrosis
    float _fibrosisFraction;                // size of fibrotic seed, 0...1, default 0.3
    float _fibrosisStromaPermeability;      // 0 = stroma not permeable, 1 = fully permeable

    // TODO: If new functions appear here, they were added automatically, move them up.
};


