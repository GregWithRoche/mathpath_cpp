#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <memory>

using Vertex2D = glm::vec2;
using Color = glm::vec3;

class Cell2D;
using SharedCell = std::shared_ptr<Cell2D>;

class Cell2D
{
public:
    enum Type
    {
        Tumor,
        Immune,
        Necrosis,
        Fibrosis,
        Unknown
    };

    Cell2D();
    Cell2D(Type type);
    Cell2D& operator=(const Cell2D& other);

    // TODO: make sure all function have an appriopriate visibility scope.
    static Cell2D createTumorCell(const Vertex2D& coordinates, const int proliferationCapacity, const bool isStem);
    static Cell2D createImmuneCell(const Vertex2D& coordinates, const unsigned proliferationCapacity, const unsigned killingCapacity);
    static Cell2D createNecrosisCell(const Vertex2D& coordinates);
    static Cell2D createFibrosisCell(const Vertex2D& coordinates);

    SharedCell proliferate(const Vertex2D& freeSpotCoordinates, const bool isProliferatingSymmetric = false);

    bool isTumor() const;
    bool isImmune() const;
    bool isNecrosis() const;
    bool isFibrosis() const;

    bool isExhausted() {return (_proliferationCapacity == 0);};
    bool isAlive() {return (_proliferationCapacity >= 0);};
    bool hasKillingCapacity() {return (_killingCapacity > 0);};

    void setDidKilling();
    bool isEngagedAfterKilling() const;
    void subtractFromKillingEngagementDuration();

    const Color& getColor() const;

    Vertex2D getPosition() const {return _position;};
    void setPosition(const Vertex2D& pos);;

    int getProliferationCapacity() const;
    void setProliferationCapacity(int capacity);

    bool isStem() const {return _isStem;};
    void setIsStem(bool isStem);

    bool diedExhausted() const;
    void setDiedExhausted();

    bool diedSpontaneously() const;
    void setDiedSpontaneously();

    bool wasKilled() const;
    void setKilled();

    static void setTumorProliferationMax(unsigned proliferationMax);
    static void setImmuneProliferationMax(unsigned proliferationMax);
    static void setImmuneKillingEngagementDuration(const unsigned duration);
    static void setImmuneKillingCapacityMax(const unsigned capacity);

    static size_t positionOffset();
    static size_t colorOffset();

    static int getTumorProliferationMax();

    void dump();

private:
    void updateColorIntensity();

    enum CellDeathReason : int
    {
        WasKilled = -3,
        DiedExhausted = -2,
        DiedSpontaneously = -1
    };

    Color    _color;
    Vertex2D _position;
    Type     _type;
    bool     _isStem;
    int      _proliferationCapacity;
    int      _killingCapacity;
    unsigned _killingEngagementDurationLeft;

    // TODO: Do this somehow else.
    static unsigned _tumorProliferationMax;
    static unsigned _immuneProliferationMax;
    static unsigned _immuneKillingCapacityMax;
    static unsigned _totalKillingEngagementDuration;
};


