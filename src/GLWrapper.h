#pragma once

// #include "glad/glad.h"
// #define GLFW_INCLUDE_NONE
#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>

class GLWrapper
{
public:
    
    // TODO: A callback may be used to resize: https://www.glfw.org/docs/latest/group__window.html#gab3fb7c3366577daef18c0023e2a8591f
    // glfwSetFramebufferSizeCallback();
    
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

    static GLuint compileShaderObject (GLenum shaderTypeGLMacro, const GLchar* shaderCode);
        
    static void drawSquare(const glm::mat4x4& projection, 
                           const glm::vec3&& translationVector, GLuint mvpLocationGLID, 
                           const glm::vec3& color, GLuint colorLocationGLID);
    
    static int initWindow(GLFWwindow* window);
    
    static GLuint createShaderProgram();
};
