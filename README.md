This is "mathpath_cpp", a project which simulates and visualizes evolution of cancer attacked by the immune system.

It depends on: opengl, GLM (https://glm.g-truc.net), GLFW (https://www.glfw.org/) and a port of GLAD contained withing GLFW. The project and packages may be built and installed on the system using a standard cmake procedure or pointed to in CMakeLists.txt and built along with mathpath_cpp.

TODO-list:
    1. Describe, what Matlab code it is based on and how.
    2. Look at usual .md files' syntax and improve it here.
    3. Add license header to each file.
    4. ...
    5. Figure out, how to get GLFW installed and use a pre-built library. And whether it is really needed?
    6. See how it is possible not to use GLAD from GLFW.
